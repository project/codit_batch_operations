<?php

declare(strict_types=1);

namespace Drupal\codit_batch_operations\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\user\UserInterface;

/**
 * Defines the BatchOpLog entity.
 *
 * @ingroup batch_op_log
 *
 * @ContentEntityType(
 *   id = "batch_op_log",
 *   label = @Translation("Batch Operation Log"),
 *   label_plural = @Translation("Batch Operation Logs"),
 *   label_collection = @Translation("Batch Operation Log"),
 *   handlers = {
 *     "storage" = "\Drupal\Core\Entity\Sql\SqlContentEntityStorage",
 *     "view_builder" = "Drupal\codit_batch_operations\BatchOpLogViewBuilder",
 *     "views_data" = "Drupal\codit_batch_operations\Entity\BatchOpLogViewsData",
 *     "form" = {
 *       "delete" = "Drupal\codit_batch_operations\Form\BatchOpLogDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\codit_batch_operations\BatchOpLogAccessControlHandler",
 *   },
 *   base_table = "batch_op_log",
 *   translatable = FALSE,
 *   admin_permission = "view codit batch operations log",
 *   entity_keys = {
 *     "id" = "id",
 *     "name" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "executor" = "executor",
 *     "created" = "created",
 *   },
 *   links = {
 *     "canonical" = "/admin/config/development/batch_operations/log/{batch_op_log}",
 *     "delete-form" = "/admin/config/development/batch_operations/log/{batch_op_log}/delete",
 *     "collection" = "/admin/config/development/batch_operations/log",
 *   },
 * )
 */
class BatchOpLog extends ContentEntityBase implements ContentEntityInterface, BatchOpLogInterface {

  /**
   * Property that prevents a BatchOpLog from saving.
   *
   * @var bool
   */
  protected $doNotSave = FALSE;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  public $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public $entityTypeManager;

  /**
   * An array of any existing log ids, keyed by BatchOperation.
   *
   * @var array
   */
  public $queriedLogIds;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values, $entity_type, $bundle = FALSE, $translations = []) {
    parent::__construct($values, $entity_type, $bundle, $translations);
    // Can't use Dependency Injection on a @ContentEntityType.
    $this->entityTypeManager = \Drupal::service('entity_type.manager');
    $this->entityFieldManager = \Drupal::service('entity_field.manager');
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values): void {
    parent::preCreate($storage, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel): array {
    $uri_route_parameters = parent::urlRouteParameters($rel);
    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function appendLog($message): BatchOpLogInterface {
    if ($message) {
      $existing_log = $this->get('log')->value;
      $this->set('log', $existing_log . PHP_EOL . $message);
      $this->setLastTime();
      $this->setMemory();
      $this->setMaxMemory();
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function appendError($message): BatchOpLogInterface {
    if ($message) {
      $existing_error = $this->get('errors')->value;
      $this->set('errors', $existing_error . PHP_EOL . $message);
      $this->setLastTime();
      $this->setMemory();
      $this->setMaxMemory();
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function canSave(): bool {
    return !$this->doNotSave;
  }

  /**
   * {@inheritdoc}
   */
  public function setDoNotSave(): BatchOpLogInterface {
    $this->doNotSave = TRUE;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    if (!$this->doNotSave) {
      return parent::save();
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setExecutor(string $executor): BatchOpLogInterface {
    $this->set('executor', $executor);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getErrors(): ?string {
    return $this->get('errors')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function hasErrors(): bool {
    return (empty($this->getErrors())) ? FALSE : TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function setCompleted(): BatchOpLogInterface {
    $this->set('completed', TRUE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCompleted(): bool {
    return (!empty($this->get('completed')->value)) ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName(string $name): BatchOpLogInterface {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $timestamp): BatchOpLogInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setLastTime(): BatchOpLogInterface {
    $this->set('last', time());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastUpdatedTime(): ?string {
    return $this->get('last')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl(): Url {
    // This avoids an edge case that occurs when a batch operation errors on the
    // first iteration.
    if (is_null($this->id())) {
      // The log has not been saved yet, but we need an id to create the link.
      // So save the log so we can have an id to work with.
      $this->save();
    }

    return Url::fromRoute('entity.batch_op_log.canonical', ['batch_op_log' => $this->id()]);
  }

  /**
   * Records the memory use in MB.
   */
  protected function setMemory():void {
    $memory = memory_get_usage(TRUE);
    $memory_in_mb = round($memory / 1048576, 1);
    $this->set('memory_use', $memory_in_mb);
  }

  /**
   * Sets the high-water mark for memory use in MB.
   */
  protected function setMaxMemory():void {
    $memory = memory_get_usage(TRUE);
    $memory_in_mb = round($memory / 1048576, 1);
    $prev_max = $this->get('max_memory')->value;
    if ($memory_in_mb > $prev_max) {
      $this->set('max_memory', $memory_in_mb);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the BatchOpLog entity.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 0,
      ])
      ->setReadOnly(TRUE);

    // Standard field.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the BatchOpLog entity.'))
      ->setReadOnly(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Script'))
      ->setDescription(t('The script that created this log.'))
      ->setRevisionable(FALSE)
      ->setSettings([
        'max_length' => 300,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      // The script name is the unique identifier so require it.
      ->setRequired(TRUE);

    $fields['executor'] = BaseFieldDefinition::create('list_string')
      ->setLabel(new TranslatableMarkup('Executor'))
      ->setDescription(new TranslatableMarkup('What method ran the script.'))
      ->setTranslatable(FALSE)
      ->setSetting('allowed_values', [
        'ui' => new TranslatableMarkup('BatchOps UI'),
        'drush' => new TranslatableMarkup('Drush'),
        'hook_update' => 'hook_update_n',
        'post_update' => new TranslatableMarkup('Post update hook'),
        'deploy' => new TranslatableMarkup('Deploy hook'),
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(FALSE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Run by'))
      ->setDescription(t('The user ID of user who ran this script.'))
      ->setRevisionable(FALSE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Started'))
      ->setDescription(t('The time that the Batch Ops Log was created / started.'))
      ->setDefaultValue(time())
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 6,
      ]);

    $fields['last'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Last iteration'))
      ->setDescription(t('The time of the last log entry for the iteration, or the time the script completed.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 8,
      ]);

    $fields['completed'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Completed'))
      ->setDescription(t('If the script was run completely.'))
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'settings' => [
          'format' => 'yes-no',
        ],
        'label' => 'inline',
        'weight' => 9,
      ]);

    $fields['total_items'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Total number of items to be processed'))
      ->setDescription(new TranslatableMarkup('The total number of items intended to be processed when this log was created.'))
      ->setTranslatable(FALSE)
      ->setSettings([
        'suffix' => ' ' . new TranslatableMarkup('items'),
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(FALSE);

    $fields['last_item_processed'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('The number of items processed.'))
      ->setDescription(new TranslatableMarkup('The count of items processed.'))
      ->setTranslatable(FALSE)
      ->setSettings([
        'suffix' => ' ' . new TranslatableMarkup('items processed'),
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number',
        'weight' => 11,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(FALSE);

    $fields['memory_use'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Most recent entry for memory used'))
      ->setDescription(new TranslatableMarkup('The most recent entry for memory used by the operation.'))
      ->setTranslatable(FALSE)
      ->setSettings([
        'suffix' => 'MB',
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number',
        'weight' => 12,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(FALSE);

    $fields['max_memory'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Max Memory Used'))
      ->setDescription(new TranslatableMarkup('The most amount of memory used by the operation.'))
      ->setTranslatable(FALSE)
      ->setSettings([
        'suffix' => 'MB',
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number',
        'weight' => 13,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(FALSE);

    $fields['log'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Log'))
      ->setDescription(new TranslatableMarkup("The log of the script run."))
      ->setTranslatable(FALSE)
      ->setRevisionable(FALSE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 14,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(FALSE);

    $fields['errors'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Errors'))
      ->setDescription(new TranslatableMarkup("Any caught errors."))
      ->setTranslatable(FALSE)
      ->setRevisionable(FALSE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 16,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(FALSE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getBatchOpLogIds(string $batch_operation_name): array {
    if (empty($this->queriedLogIds[$batch_operation_name])) {
      // We don't already have it, so lets go get it.
      $this->queriedLogIds[$batch_operation_name] = $this->entityTypeManager
        ->getStorage('batch_op_log')
        ->getQuery()
        ->condition('name', $batch_operation_name, '=')
        ->accessCheck(TRUE)
        ->execute();
    }
    return $this->queriedLogIds[$batch_operation_name] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getMostRecentBatchOpLog(string $batch_operation_name): ?BatchOpLog {
    $entity_ids = $this->getBatchOpLogIds($batch_operation_name);
    if (!empty($entity_ids)) {
      $most_recent_id = end($entity_ids);
      $most_recent_log = $this->load($most_recent_id);
    }
    return $most_recent_log ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getRelatedBatchOpLogs(string $batch_operation_name): array {
    $entity_ids = $this->getBatchOpLogIds($batch_operation_name);
    if (!empty($entity_ids)) {
      $batch_op_logs = $this->loadMultiple($entity_ids);
    }
    return $batch_op_logs ?? [];
  }

}
