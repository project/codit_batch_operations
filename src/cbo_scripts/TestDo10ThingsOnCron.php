<?php

namespace Drupal\codit_batch_operations\cbo_scripts;

use Drupal\codit_batch_operations\BatchOperations;
use Drupal\codit_batch_operations\BatchScriptInterface;

/**
 * A test and example Batch operation script to show processing.
 */
class TestDo10ThingsOnCron extends BatchOperations implements BatchScriptInterface {

  /**
   * {@inheritdoc}
   */
  public function getTitle():string {
    return 'Do 10 things running on cron, but not actually do anything other than log things.';
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription():string {
    $description = <<<ENDHERE
    This is intended as an example for writing a BatchOperation that runs using
    cron.  It pretends to wake up a groundhog on Groundhog's day and July 4th
    because groundhogs love a good fireworks display.
    ENDHERE;
    return $description;
  }

  /**
   * {@inheritdoc}
   */
  public function getCompletedMessage(): string {
    // This message can include the tokens '@completed' and '@total'.
    return '@completed times we called the groundhog out of @total times. He never showed.';
  }

  /**
   * {@inheritdoc}
   */
  public function gatherItemsToProcess(): array {
    // Do whatever you need to here to put together the list of items
    // to be processed. Can be a keyed array like
    // [key1 => item1, key2 => item2 ...]
    // or a flat array [item1, item2, item3 ...].
    // 'item' can be something simple like a node id for processOne() to load,
    // or could be a loaded entity to act on.
    $items = array_fill(1, 10, 'Here groundhog, come on boy.');
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function processOne(string $key, mixed $item, array &$sandbox): string {
    // Do some things in here, then return a message about what was done.
    // If you return a non-empty message, it will get logged in the BatchOpLog.
    // If you were doing a big process and wanted to add to the log or errors,
    // you can log specifically as you go.
    return "I called '{$item}'";
  }

  /**
   * {@inheritdoc}
   */
  public function getCronTiming(): string | array {
    return [
      'on the 2nd of February after 08:00',
      'on the 4th of July after 21:00',
    ];
  }

}

// @codingStandardsIgnoreStart
// Example of how to run this batch from a hook_update_n()
/**
 * Run a script example with coconuts.
 */
// function my_module_update_9012(&$sandbox) {
//  $script = \Drupal::classResolver('\Drupal\codit_batch_operations\cbo_scripts\TestDo10ThingsOnCron');
//  return $script->run($sandbox, 'hook_update');
// }
//
//
// Run with drush:
// drush codit-batch-operations:run TestDo10ThingsOnCron
// @codingStandardsIgnoreEnd
