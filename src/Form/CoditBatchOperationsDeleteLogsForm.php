<?php

namespace Drupal\codit_batch_operations\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Form to initiate the running of a BatchOperation.
 */
class CoditBatchOperationsDeleteLogsForm extends ConfirmFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger channel factory.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $dMessenger;

  /**
   * Constructor for the Form.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory interface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    LoggerChannelFactoryInterface $logger_factory,
    MessengerInterface $messenger,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger_factory->get('codit_batch_operations');
    $this->dMessenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('messenger'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'batch_operation_delete_all_logs';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get a count of all the logs.
    $vars = [
      '@count' => count($this->getBatchOpLogIds()),
    ];
    $form['warning'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => $this->t('Warning'),
    ];
    $form['notice'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('You are about delete all @count batch operation logs. You will lose all that history.', $vars),
    ];

    $form['explanation'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Deleting them all will make it so that BatchOperations that are restricted to have only run once, can now be run again.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Prep of the deletion of BatchOpLogs.
    $ids = $this->getBatchOpLogIds();
    // BatchOpLogs have the potential to be quite large so we should not load
    // them all at once. Batch them.
    $operations = [];
    foreach ($ids as $idInt => $idString) {
      $operations[] = [[$this, 'deleteOneBatchOpLog'], [$idInt]];
    }

    $batch = [
      'operations' => $operations,
      'progressive' => FALSE,
      'finished' => [$this, 'finishedBatchDelete'],
      'title' => t('Deleting @name', ['@name' => 'BatchOpLog']),
      'init_message' => t('Preparing for removal of every @name.', ['@name' => 'BatchOpLog']),
      'progress_message' => t('Deletion of @name in progress. (elapsed time: @elapsed) (estimated time remaining: @estimate)', ['@name' => 'BatchOpLog']),
      'error_message' => t('Deletion of every @name has encountered an error.', ['@name' => 'BatchOpLog']),
    ];
    batch_set($batch);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('codit_batch_operations.config_form');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Delete all batch operation logs?');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Yes, delete them all');
  }

  /**
   * Get all the ids for existing BatchOpLogs.
   *
   * @return array
   *   An array of numeric id keys and matching string values.
   */
  protected function getBatchOpLogIds(): array {
    $storage_handler = $this->entityTypeManager->getStorage('batch_op_log');
    $query = $storage_handler->getQuery();
    $query->accessCheck(FALSE);
    return $query->execute();
  }

  /**
   * Callback to the batch api operation to delete a BatchOpLog.
   *
   * @param int $id
   *   The BatchOpLog id to delete.
   * @param array $context
   *   A context array containing the sandbox for keeping state across runs.
   */
  public function deleteOneBatchOpLog(int $id, array &$context): void {
    $storage_handler = $this->entityTypeManager->getStorage('batch_op_log');
    $batch_op_log = $storage_handler->load($id);
    $storage_handler->delete([$batch_op_log]);
    $context['results']['deleted'][] = $id;
  }

  /**
   * The callback that gets called when BatchOpLog deletion completes.
   *
   * @param bool $success
   *   TRUE if the batch completed.  FALSE otherwise.
   * @param array $results
   *   Storage for items assigned during the run.
   * @param mixed $operations
   *   A way to track uncompleted items.
   * @param string $elapsed
   *   The elapsed time with units.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect to go to the Batch operation page.
   */
  public function finishedBatchDelete($success, $results, $operations, $elapsed): RedirectResponse {
    $deleted_count = count($results['deleted']);
    $vars = [
      '@deleted_count' => $deleted_count,
    ];
    $this->dMessenger->addMessage($this->t('@deleted_count BatchOpLogs were deleted.', $vars));
    $this->logger->info('@deleted_count BatchOpLogs were deleted.', $vars);

    return new RedirectResponse(Url::fromRoute('codit_batch_operations.config_form')->toString());
  }

}
