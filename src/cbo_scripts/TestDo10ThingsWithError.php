<?php

namespace Drupal\codit_batch_operations\cbo_scripts;

use Drupal\codit_batch_operations\BatchOperations;
use Drupal\codit_batch_operations\BatchScriptInterface;

/**
 * A test and example Batch operation script to show processing.
 */
class TestDo10ThingsWithError extends BatchOperations implements BatchScriptInterface {

  /**
   * {@inheritdoc}
   */
  public function getTitle():string {
    return 'Do 10 things and have an error, but not actually do anything other than count sheep';
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription():string {
    $description = <<<ENDHERE
    This script intentionally includes <strong>2 fatal errors</strong>.  Try
    running it with and without allow-skip so you understand the difference.
    Beyond trying out the inner workings of Codit Batch Operations this
    BatchOperation does nothing but count sheep.
    ENDHERE;

    return $description;
  }

  /**
   * {@inheritdoc}
   */
  public function getCompletedMessage(): string {
    // This message can include the tokens '@completed' and '@total'.
    return 'The script processed @completed out of @total items.';
  }

  /**
   * {@inheritdoc}
   */
  public function getItemType(): string {
    return 'sheep';
  }

  /**
   * {@inheritdoc}
   */
  public function gatherItemsToProcess(): array {
    // Do whatever you need to put together the list of items
    // to be processed. Can be a keyed array like
    // [key1 => item1, key2 => item2 ...]
    // or a flat array [item1, item2, item3 ...].
    // 'item' can be something simple like a node id for processOne() to load,
    // or could be a loaded entity act on.
    $items = [
      '7' => 'One',
      '8' => 'Two',
      '15' => 'Three',
      '27' => 'Four',
      '28' => 'Five',
      '29' => 'Six',
      '41' => 'Seven',
      '46' => 'Eight',
      '57' => 'Nine',
      '100' => 'Ten',
    ];

    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function processOne(string $key, mixed $item, array &$sandbox): string {
    // Do some things in here, then return a message about what was done.
    // If you return a non-empty message, it will get logged in the BatchOpLog.
    // If you were doing a big process and wanted to add to the log or errors
    // you can log specifically as you go.
    if ($key === 'sheep_29') {
      // Call a non-existent function to force an error.
      // @phpstan-ignore-next-line
      someFunctionThatDoesNotExist();
    }
    if ($key === 'sheep_46') {
      // Call a different non-existent function to force a different error.
      // @phpstan-ignore-next-line
      someOtherMistake();
    }

    return "{$item} sheep.";
  }

}

// @codingStandardsIgnoreStart
// Example of how to run this batch from a hook_update_n()
/**
 * Run a script example with errors and 10 items.
 */
// function my_module_update_9012(&$sandbox) {
//  $script = \Drupal::classResolver('\Drupal\codit_batch_operations\cbo_scripts\TestDo10ThingsWithError');
//  return $script->run($sandbox, 'hook_update');
// }
//
//
// Run with drush:
// drush codit-batch-operations:run TestDo10ThingsWithError
// @codingStandardsIgnoreEnd
