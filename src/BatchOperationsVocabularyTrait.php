<?php

namespace Drupal\codit_batch_operations;

use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermStorageInterface;

/**
 * Common node related methods for optional use during BatchOperations.
 */
trait BatchOperationsVocabularyTrait {

  /**
   * Get the term storage.
   *
   * @return \Drupal\taxonomy\TermStorageInterface
   *   Term storage.
   */
  public function getTermStorage(): TermStorageInterface {
    return $this->entityTypeManager->getStorage('taxonomy_term');
  }

  /**
   * Create new terms for if they do not exist.
   *
   * @param string $vocabulary_id
   *   The machine name of the taxonomy vocabulary.
   * @param array $terms
   *   An array of terms in the form of 'term name' => 'description'.
   *
   * @return int
   *   The number of terms created.
   */
  public function saveNewTerms($vocabulary_id, array $terms): int {
    $terms_created = 0;
    foreach ($terms as $name => $description) {
      // Make sure we are not creating duplicate terms.
      $tid = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery()
        ->condition('name', $name)
        ->condition('vid', $vocabulary_id)
        ->accessCheck(FALSE)
        ->execute();
      if (empty($tid)) {
        // Term does not exist, so create it.
        $term = Term::create([
          'name' => $name,
          'vid' => $vocabulary_id,
        ]);
        $term->setNewRevision(TRUE);
        $term->setDescription($description);
        $term->setRevisionUserId($this->getUser());
        $term->setSyncing(TRUE);
        $term->setValidationRequired(FALSE);
        $term->save();
        $terms_created++;
      }
    }
    return $terms_created;
  }

}
