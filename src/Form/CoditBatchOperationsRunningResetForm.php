<?php

namespace Drupal\codit_batch_operations\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\State;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to reset the running state of BatchOperations.
 */
class CoditBatchOperationsRunningResetForm extends ConfirmFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger channel factory.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $dMessenger;

  /**
   * The state api service.
   *
   * @var \Drupal\Core\State\State
   */
  public $state;

  /**
   * Constructor for the Form.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory interface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\State\State $state
   *   The State API service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    LoggerChannelFactoryInterface $logger_factory,
    MessengerInterface $messenger,
    State $state,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger_factory->get('codit_batch_operations');
    $this->dMessenger = $messenger;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'batch_operation_running_reset';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $running = $this->state->get('cbo_running_script', NULL);
    $vars = [
      '@running' => $running,
    ];
    if (empty($running)) {
      $form['warning'] = [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#value' => $this->t('There are no Batch Operations listed as running, please cancel.'),
      ];
    }
    else {
      $form['warning'] = [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#value' => $this->t('Warning'),
      ];
      $form['notice'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t("You are about to reset the running state of a Batch Operation which currently shows '@running' as running", $vars),
      ];

      $form['explanation'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('Only proceed if you are sure the Batch Operation is not running.'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $running = $this->state->get('cbo_running_script', NULL);
    if ($running) {
      $vars = [
        '@running' => $running,
      ];
      $this->state->set('cbo_running_script', NULL);
      $this->logger->info("The running state of '@running' was removed.", $vars);
    }
    $form_state->setRedirectUrl(Url::fromRoute('codit_batch_operations.config_form'));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('codit_batch_operations.config_form');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Reset the Batch Operation running state?');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Yes, tell the system that the Batch Operation is not running.');
  }

}
