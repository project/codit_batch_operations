<?php

namespace Drupal\codit_batch_operations\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting BatchOpLog entities.
 *
 * @ingroup batch_op_log
 */
class BatchOpLogDeleteForm extends ContentEntityDeleteForm {

}
