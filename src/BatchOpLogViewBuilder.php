<?php

namespace Drupal\codit_batch_operations;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Defines a class for entity view builder for entities.
 */
class BatchOpLogViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $batch_op_log, $view_mode = 'full', $langcode = NULL) {
    $build = parent::view($batch_op_log, $view_mode, $langcode);

    return $build;
  }

}
