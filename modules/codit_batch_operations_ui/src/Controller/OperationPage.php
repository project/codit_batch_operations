<?php

namespace Drupal\codit_batch_operations_ui\Controller;

use Drupal\codit_batch_operations\BatchOperationsFilesTrait;
use Drupal\codit_batch_operations\Entity\BatchOpLog;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\Core\Utility\TableSort;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class Operations Controller.
 *
 * @package Drupal\codit_batch_operations_ui\Controller
 */
class OperationPage extends OperationsBase {

  use BatchOperationsFilesTrait;

  /**
   * Renders the batch operation page based on the parameter in the URI.
   *
   * @param string $batch_operation_name
   *   The class name of the batch operation from URI parameter.
   *
   * @return array
   *   A render array for the batch operation page.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   In the case where the parameter does not match an existing BatchOp class.
   */
  public function renderOperation(string $batch_operation_name): array {
    if (!$this->isBatchOperation($batch_operation_name)) {
      // Does not exist, return 404.
      throw new NotFoundHttpException();
    }
    $page = [];
    $page['page'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'batch-operation',
        ],
      ],
    ];
    $batch_op_class_name = $this->getNamespacedClassName($batch_operation_name);
    $batch_operation = $this->getBatchOperationClass($batch_operation_name);

    $page['page']['title'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => $batch_operation->getTitle(),
    ];
    $description = $batch_operation->getDescription();
    if (!empty($description)) {
      $page['page']['description'] = [
        'label' => [
          '#type' => 'label',
          '#title' => $this->t('Description'),
          '#title_display' => 'above',
        ],
        'value' => [
          '#type' => 'container',
          '#markup' => "<p class=\"description\">{$batch_operation->getDescription()}</p>",
        ],
      ];
    }

    $batch_size = $batch_operation->getBatchSize();
    // Removing this until it is better defined in purpose and procedure.
    // Only show it for legacy batches that are not using default of 1.
    if ($batch_size > 1) {
      $batch_size = number_format($batch_size);
      $batch_size_text = new PluralTranslatableMarkup(
          $batch_size,
          '1 item at a time',
          '@count items at a time'
        );
      $label = $this->t('Batch size:');
      $page['page']['batch_size'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => "<span class=\"item__label\">$label</span> <span>$batch_size_text</span>",
      ];
    }

    $item_count_raw = $batch_operation->getTotalItemCount();
    $item_count = number_format($item_count_raw);
    $item_count_text = new PluralTranslatableMarkup(
        $item_count,
        '1 item',
        '@count items '
      );
    $label = $this->t('Total items to process:');
    $page['page']['item_count'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => "<span class=\"item__label\">$label</span> <span>$item_count_text</span>",
    ];

    if ($batch_operation->getAllowOnlyOneCompleteRun()) {
      $allow_label = $this->t('Can only be run to completion once');
      $affirmative = $this->t('TRUE');
      $page['page']['only-once'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => "<span class=\"item__label\">$allow_label:</span> <span>$affirmative</span>",
      ];
    }

    $cron_time = ($batch_operation->getCronTiming()) ? $batch_operation->getCronTiming() : $this->t('Not set to run on cron.');
    $cron_label = $this->t('Cron timing:');
    if (is_array($cron_time) && (count($cron_time) > 1)) {
      // Format it as a list.
      $page['page']['cron'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#title' => $cron_label,
        '#value' => "<span class=\"item__label\">$cron_label</span>",
      ];
      $page['page']['cron-list'] = [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#items' => $cron_time,
        '#attributes' => ['class' => 'cron-list'],
        '#wrapper_attributes' => ['class' => 'container'],
      ];
    }
    else {
      // If it is an array with only one item, make it act like a string.
      $cron_time = (is_array($cron_time)) ? reset($cron_time) : $cron_time;
      $page['page']['cron'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#title' => $cron_label,
        '#value' => "<span class=\"item__label\">$cron_label</span> <span>$cron_time</span>",
      ];
    }

    if (!empty($batch_operation->getCronTiming())) {
      $cron_next_run_label = $this->t('This will run after:');
      $next_time = $batch_operation->getNextCronShouldRun();
      $page['page']['cron-next-run'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#title' => $cron_next_run_label,
        '#value' => "<span class=\"item__label\">$cron_next_run_label</span> <span>$next_time</span>",
      ];
    }

    $page['breaker'] = [
      '#markup' => '<hr>',
    ];

    $page['form'] = $this->formBuilder->getForm('\Drupal\codit_batch_operations_ui\Form\BatchOperationRun', $this->getNamespacedClassName($batch_operation_name), $batch_operation->getTitle());
    // Hide the form if it can only be run once and has already run.
    if ($batch_operation->getAllowOnlyOneCompleteRun() && !empty($batch_operation->getBatchOpLog()->getMostRecentBatchOpLog($batch_operation::class))) {
      if ($batch_operation->getBatchOpLog()->getMostRecentBatchOpLog($batch_operation::class)->getCompleted()) {
        unset($page['form']);
      }
    }

    $page['table-breaker'] = [
      '#markup' => '<hr>',
    ];
    $header = [
      ['data' => $this->t('Run date'), 'field' => 'run-date', 'sort' => 'desc'],
      ['data' => $this->t('Duration'), 'field' => 'duration'],
      ['data' => $this->t('Run method'), 'field' => 'run-method'],
      ['data' => $this->t('Run by'), 'field' => 'run-by'],
      ['data' => $this->t('Status'), 'field' => 'status'],
      ['data' => $this->t('Operation'), 'field' => 'operation'],
    ];
    $rows = $this->assembleRows($batch_op_class_name, $item_count_raw);
    $order = TableSort::getOrder($header, $this->request);
    $direction = TableSort::getSort($header, $this->request);
    $rows = $this->sortRows($rows, $order['sql'], $direction);
    $rows = $this->convertDuration($rows);
    $rows = $this->convertDate($rows);
    $page['log_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#caption' => $this->t('Logs of previous runs.'),
      '#rows' => $rows,
      '#empty' => $this->t('This has no previous logs.'),
    ];
    $page['pager'] = [
      '#type' => 'pager',
    ];
    return $page;
  }

  /**
   * Renders the batch operation page based on the parameter in the URI.
   *
   * @param string $batch_operation_name
   *   The class name of the batch operation from URI parameter.
   * @param string $skip
   *   Whether should 'fail' or 'allow_skip'.
   *
   * @return array
   *   A render array for the batch operation page.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   In the case where the parameter does not match an existing BatchOp class.
   */
  public function renderOperationConfirmation(string $batch_operation_name, string $skip): array {
    if (!$this->isBatchOperation($batch_operation_name) || !$this->isValidSkipOption($skip)) {
      // Does not exist, return 404.
      throw new NotFoundHttpException();
    }
    $form = $this->formBuilder->getForm('\Drupal\codit_batch_operations_ui\Form\BatchOperationRunConfirmation', $this->getNamespacedClassName($batch_operation_name), $skip);

    return $form;
  }

  /**
   * Checks to see if the arg is valid.
   *
   * @param string $skip_arg
   *   The arg to check.
   *
   * @return bool
   *   TRUE if the arg is valid, FALSE otherwise.
   */
  protected function isValidSkipOption(string $skip_arg) : bool {
    $valid = ['skip', 'fail'];
    return (in_array($skip_arg, $valid)) ? TRUE : FALSE;
  }

  /**
   * Page title callback.
   *
   * @param string $batch_operation_name
   *   The name of the operation, from the URI parameter.
   *
   * @return string
   *   The fully translated title.
   */
  public function getTitle(string $batch_operation_name): string {
    $vars = [
      '@name' => $batch_operation_name,
    ];
    return $this->t('Operation: @name', $vars);
  }

  /**
   * Assemble the list rows, one per BatchOpLog.
   *
   * @param string $batch_operation_name
   *   The class name of a batch operation.
   * @param ?int $operation_size
   *   The current items in the BatchOperation.
   *
   * @return array
   *   An array of rows.
   */
  protected function assembleRows(string $batch_operation_name, $operation_size): array {
    $rows = [];
    $batchOpLog = new BatchOpLog([], 'batch_op_log');
    $batch_op_logs = $batchOpLog->getRelatedBatchOpLogs(trim($batch_operation_name, '\\'));

    foreach ($batch_op_logs as $batch_op_log) {
      $row = [
        'run-date' => ($batch_op_log) ? $batch_op_log->get('last')->value : '-',
        'duration' => $batch_op_log->get('last')->value - $batch_op_log->get('created')->value,
        'run-method' => ($batch_op_log) ? $batch_op_log->get('executor')->value : '-',
        'run-by' => $batch_op_log->get('user_id')->entity->toLink($batch_op_log->get('user_id')->entity->get('name')->value),
        'status' => $this->buildCompleteStatus($batch_op_log, $operation_size),
        'operation' => new Link($this->t('View log'), $batch_op_log->getUrl()),
      ];
      $rows[] = $row;
    }

    return $rows;
  }

}
