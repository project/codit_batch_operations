<?php

namespace Drupal\codit_batch_operations\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\State;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the module's config/settings admin page.
 */
final class CoditBatchOperationsConfigForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Module handler interface.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private ModuleHandlerInterface $moduleHandler;

  /**
   * The state api service.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * CoditBatchOperationsConfigForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler for determining which modules are installed.
   * @param \Drupal\Core\State\State $state
   *   The State API service.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed configuration manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $moduleHandler, State $state, TypedConfigManagerInterface $typed_config_manager) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->moduleHandler = $moduleHandler;
    $this->entityTypeManager = $entity_type_manager;
    $this->state = $state;
    $this->typedConfigManager = $typed_config_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('state'),
      $container->get('config.typed') ?? NULL,
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'codit_batch_operations.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'codit_batch_operations_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('codit_batch_operations.settings');

    $script_location = $config->get('script_location');
    if (empty($script_location)) {
      $description = $this->t('Enter the machine name of the local module where you would like to commit your scripts. Leaving this blank will show only test Batch Operations.');
    }
    elseif ($this->moduleHandler->moduleExists($script_location)) {
      $description = $this->t('The module "@module_name" will contain any scripts at @module_name/src/cbo_scripts', ['@module_name' => $script_location]);
    }
    else {
      $description = $this->t('The module "@module_name" could not be found.  Make sure it is enabled.', ['@module_name' => $script_location]);
    }

    $form['script_location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Machine name of local module to commit your custom script files'),
      '#description' => $description,
      '#default_value' => $config->get('script_location'),
      '#access' => TRUE,
    ];

    $default_user = $config->get('default_user');
    if (empty($default_user)) {
      $user_description = $this->t('This is a numeric user id that will be attributed to any entity saves performed by the script.');
    }
    else {
      $account = $this->entityTypeManager->getStorage('user')->load($default_user);
      if (!empty($account)) {
        $name = $account->getDisplayName();
        $user_description = $this->t("The user '@name' will be attributed to any entity saves or other operations performed by the script.", ['@name' => $name]);
      }
      else {
        $user_description = $this->t('The user does not exist.  Please use a different user id.');
      }
    }

    $form['default_user'] = [
      '#type' => 'number',
      '#title' => $this->t('User ID to use to record as the operator in the script'),
      '#description' => $user_description,
      '#default_value' => $config->get('default_user'),
      '#access' => TRUE,
    ];

    $cron_description = $this->t('Only BatchOperations that have the optional getCronTiming() defined will run on cron if this is enabled.');
    $form['cron_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable cron processing of any BatchOperation with getCronTiming() defined.'),
      '#description' => $cron_description,
      '#default_value' => $config->get('cron_enabled'),
      '#access' => TRUE,
    ];
    $running = $this->state->get('cbo_running_script', NULL);
    if ($running) {
      // Something is running, so offer the link to unset it.
      $form['reset_running'] = [
        '#type' => 'link',
        '#title' => $this->t("Reset the state of Batch Operation '@running' to not be running.", ['@running' => $running]),
        '#url' => Url::fromRoute('codit_batch_operations.running_reset_form'),
        '#prefix' => "<hr>{$this->t('A Batch Operation is listed as running.')}<br>",
        '#suffix' => " (<strong>{$this->t('Caution')}</strong>)",
        '#weight' => 400,
      ];
    }

    $form['delete_logs'] = [
      '#type' => 'link',
      '#title' => $this->t('Delete all batch operation logs'),
      '#url' => Url::fromRoute('codit_batch_operations.delete_log_form'),
      '#prefix' => '<hr>',
      '#suffix' => " (<strong>{$this->t('Caution')}</strong>)",
      '#weight' => 500,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);
    $config = $this->config('codit_batch_operations.settings');
    $config->set('script_location', $form_state->getValue('script_location'));
    $config->set('default_user', $form_state->getValue('default_user'));
    $config->set('cron_enabled', $form_state->getValue('cron_enabled'));
    $config->save();
  }

}
