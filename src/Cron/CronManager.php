<?php

namespace Drupal\codit_batch_operations\Cron;

use Drupal\codit_batch_operations\BatchOperations;
use Drupal\Core\Database\Query\Merge;

/**
 * Is used to track time related to cron jobs and their table storage.
 */
class CronManager implements CronManagerInterface {

  /**
   * The BatchOperations class.
   *
   * @var \Drupal\codit_batch_operations\BatchOperations
   */
  public $batchOperations;

  /**
   * The name of this BatchOperation.
   *
   * @var string
   */
  protected $batchOperationName;

  /**
   * This is the time when cron last ran this function.
   *
   * @var int
   */
  protected $cronLastRan;


  /**
   * This is the time that the cron should next run.
   *
   * @var int
   */
  protected $cronShouldRunAfter;

  /**
   * Flag for making sure this runs now if it has never run before.
   *
   * @var ?bool
   */
  protected $firstRun = NULL;

  /**
   * Flag for identifying if this is an 'every' timing pattern.
   *
   * @var bool
   */
  public $needsForceRunToInstantiate = FALSE;

  /**
   * AfterTime. As in '... after {02:15}'.
   *
   * @var ?string
   */
  protected $patternAfterTime;

  /**
   * The day if there is one, NULL if there is not.
   *
   * @var ?string
   */
  protected $patternDay;


  /**
   * Any month from the timing string.  As in 'after 4th of {July}'.
   *
   * @var ?string
   */
  protected $patternMonth;


  /**
   * SkipQty.  As in 'every {3} days...'.
   *
   * @var mixed
   */
  protected $patternSkipQty;

  /**
   * SkipUnit.  As in 'every 3 {days} ...'.
   *
   * @var ?string
   */
  protected $patternSkipUnit;

  /**
   * The cron time pattern submitted and cleaned up.
   *
   * @var string
   */
  protected $patternSubmitted;

  /**
   * Array of allowed months with variation on spellings.
   *
   * @var array
   */
  const ALLOWED_MONTHS = [
    'jan' => 'january',
    'january' => 'january',
    'feb' => 'february',
    'february' => 'february',
    'mar' => 'march',
    'march' => 'march',
    'apr' => 'april',
    'april' => 'april',
    'may' => 'may',
    'jun' => 'june',
    'june' => 'june',
    'jul' => 'july',
    'july' => 'july',
    'aug' => 'august',
    'august' => 'august',
    'sep' => 'september',
    'sept' => 'september',
    'september' => 'september',
    'oct' => 'october',
    'october' => 'october',
    'nov' => 'november',
    'november' => 'november',
    'dec' => 'december',
    'december' => 'december',
  ];

  /**
   * Array of allowed time units with variation on spellings and plural.
   *
   * @var array
   */
  const ALLOWED_UNITS = [
    'second' => 'seconds',
    'seconds' => 'seconds',
    'sec' => 'seconds',
    'minute' => 'minutes',
    'minutes' => 'minutes',
    'min' => 'minutes',
    'days' => 'days',
    'day' => 'days',
    'hour' => 'hours',
    'hours' => 'hours',
    'hr' => 'hours',
    'hrs' => 'hours',
    'weeks' => 'weeks',
    'week' => 'weeks',
    'wk' => 'weeks',
    'wks' => 'weeks',
    'months' => 'months',
    'month' => 'months',
    'years' => 'years',
    'year' => 'years',
    'yr' => 'years',
    'yrs' => 'years',
  ];

  /**
   * Array of maximum number of days in each month.
   *
   * Risk: February leap year or not.
   *
   * @var array
   */
  const MAX_DAYS_IN_MONTH = [
    'january' => 31,
    'february' => 29,
    'march' => 31,
    'april' => 30,
    'may' => 31,
    'june' => 30,
    'july' => 31,
    'august' => 31,
    'september' => 30,
    'october' => 31,
    'november' => 30,
    'december' => 31,
  ];

  /**
   * Builds CronManager upon instantiation.
   *
   * @param string $batch_operation_name
   *   The machine name of the cron function.
   * @param string $cron_timing
   *   Timing string to be parsed, see README.md for patterns.
   * @param \Drupal\codit_batch_operations\BatchOperations $batch_operations
   *   The BatchOperations object that is being run by the cron.
   */
  public function __construct(string $batch_operation_name, string $cron_timing, BatchOperations $batch_operations) {
    $this->batchOperations = $batch_operations;
    $this->setBatchOperationName($batch_operation_name);
    $this->setPatternSubmitted($cron_timing);
  }

  /**
   * Setter for BatchOperationName.
   *
   * @param string $batch_operation_name
   *   The unique machine name for the BatchOperation class (short name).
   *
   * @return CronManager
   *   For chaining.
   *
   * @throws Exception
   */
  protected function setBatchOperationName($batch_operation_name = ''): CronManager {
    if (!empty($batch_operation_name) && (strlen($batch_operation_name) <= 255)) {
      $this->batchOperationName = $batch_operation_name;
    }
    else {
      $msg_vars = ['!BatchOperationName' => $batch_operation_name];
      throw new \Exception(t('Can not instantiate a CronTime object without passing a valid BatchOperation name that is 255 characters or less.', $msg_vars));
    }
    return $this;
  }

  /**
   * Getter for the BatchOperationName.
   *
   * @return string
   *   The cron name of this cron function.
   */
  public function getBatchOperationName(): string {
    return $this->batchOperationName;
  }

  /**
   * Setter for the timing string pattern that also parses the string.
   *
   * @param string $pattern
   *   A valid timing string. See Readme.md for supported timing strings.
   *
   * @return CronManager
   *   For chaining.
   */
  protected function setPatternSubmitted($pattern): CronManager {
    if (!empty($pattern)) {
      $pattern = strtolower(trim($pattern));
      // Remove multiple spaces.
      $pattern = preg_replace('!\s+!', ' ', $pattern);
      $this->patternSubmitted = $pattern;

      // Now that the pattern is set, go ahead and parse it.
      // Extracting for skip_qty, skip_units, time, day, month.
      $time_pieces = explode(' ', $this->getPatternSubmitted());
      $this->parsePatternMonth($time_pieces);
      $this->parsePatternSkipQty($time_pieces);
      $this->parsePatternSkipUnit($time_pieces);
      $this->parsePatternAfterTime($time_pieces);
      $this->parsePatternDay($time_pieces);

      // Use the parsed info to build the time.
      $this->getNextRun();
    }

    return $this;
  }

  /**
   * Getter for submitted timing string pattern.
   *
   * @return string
   *   The timing string pattern submitted.
   */
  public function getPatternSubmitted(): string {
    return $this->patternSubmitted;
  }

  /**
   * Getter for the SkipUnit from the timing string.
   *
   * @return ?string
   *   - string Timing unit like seconds, minutes, hours, days, months, years.
   *   - NULL if unavailable.
   */
  public function getPatternSkipUnit(): ?string {
    return $this->patternSkipUnit;
  }

  /**
   * Evaluates a time pattern and extracts and sets the unit of time.
   *
   * @param array $time_pieces
   *   Array of exploded time string containing a unit.
   */
  protected function parsePatternSkipUnit(array $time_pieces = []): void {
    // The skip unit should appear 2 index points after the word 'every'.
    $parsed_unit = $this->parseRetriever('every', $time_pieces, 2);
    $this->setPatternSkipUnit($parsed_unit);
  }

  /**
   * Setter for valid PatternSkipUnit. As in 'every 6 {PatternSkipUnit}'.
   *
   * @param ?string $possible_unit
   *   A unit defining the amount of time (seconds, hours, days, weeks...).
   *
   * @return CronManager
   *   For chaining.
   */
  protected function setPatternSkipUnit(?string $possible_unit = ''): CronManager {
    if (!empty($possible_unit)) {
      $possible_unit = strtolower(trim($possible_unit));
      // Check to see if $possible_unit is an allowed unit.
      $unit = (!empty(self::ALLOWED_UNITS[$possible_unit])) ? self::ALLOWED_UNITS[$possible_unit] : NULL;
    }
    $this->patternSkipUnit = (empty($unit)) ? NULL : $unit;
    return $this;
  }

  /**
   * Setter for Month to set in timing string.
   *
   * @param string $possible_month
   *   The month to set in the timing of the cron function.
   *
   * @return CronManager
   *   For chaining.
   */
  protected function setPatternMonth(string $possible_month = ''): CronManager {
    // Check to see that month name is present.
    if (!empty($possible_month) && !empty(self::ALLOWED_MONTHS[$possible_month])) {
      $this->patternMonth = self::ALLOWED_MONTHS[$possible_month];
    }
    return $this;
  }

  /**
   * Getter for PatternMonth from timing string.  As in 'after 4th of {July}'.
   *
   * @return ?string
   *   - string The name of the month set.
   *   - NULL If no month is set.
   */
  public function getPatternMonth(): ?string {
    return $this->patternMonth ?? NULL;
  }

  /**
   * Finds a month and sets the integer representation of that month if found.
   *
   * @param array $time_pieces
   *   An array of parts that make up a time phrase.
   */
  private function parsePatternMonth(array $time_pieces = []): void {
    foreach ($time_pieces as $key => $value) {
      if (!empty(self::ALLOWED_MONTHS[$value])) {
        $this->setPatternMonth($value);
      }
    }
  }

  /**
   * Setter for the SkipQty.  As in 'every {3} days...'.
   *
   * @param mixed $possible_qty
   *   The number of units to skip each time.
   *
   * @return CronManager
   *   For chaining.
   */
  protected function setPatternSkipQty($possible_qty = NULL): CronManager {
    $qty = NULL;
    // Make sure its an integer.
    if (!empty($possible_qty)) {
      settype($possible_qty, "integer");
      $qty = ((!empty($possible_qty)) && ($possible_qty > 0)) ? $possible_qty : NULL;
    }
    $this->patternSkipQty = $qty;
    return $this;
  }

  /**
   * Getter for the SkipQty.
   *
   * @return ?int
   *   - int The number of units to skip each time.
   *   - NULL If there are no units to skip.
   */
  public function getPatternSkipQty(): ?int {
    return $this->patternSkipQty ?? NULL;
  }

  /**
   * Parses and sets the skip quantity.
   *
   * @param array $time_pieces
   *   An array of parts that make up a time phrase.
   */
  protected function parsePatternSkipQty(array $time_pieces = []): void {
    $qty = NULL;
    // The skip qty will always appear after 'every'.
    // Find 'every'.
    $parsed_qty = $this->parseRetriever('every', $time_pieces, 1);
    $this->setPatternSkipQty($parsed_qty);
  }

  /**
   * Setter for the AfterTime.  As in '... after {02:15}'.
   *
   * @param ?string $possible_time
   *   A string of the 24 hour format time after which the cron function runs.
   *
   * @return CronManager
   *   For chaining.
   */
  protected function setPatternAfterTime(?string $possible_time = ''): CronManager {
    if (!empty($possible_time)) {
      // Need to sanitize for HH:MM:SS.
      $time_components = date_parse($possible_time);
      if ($time_components['error_count'] !== 0) {
        $possible_time = NULL;
        // Log message.
        foreach ($time_components['errors'] as $error) {
          $vars = ['@crontime' => $possible_time, '@cronerror' => $error];
          $this->batchOperations->logger->error('The setPatternAfterTime() did not like the time of "@crontime" due to @cronerror.', $vars);
        }
      }
    }
    $this->patternAfterTime = $possible_time;
    return $this;
  }

  /**
   * Getter for AfterTime. As in '... after {02:15}'.
   *
   * @return ?string
   *   - string The 24 hour format time after which the cron function runs.
   *   - NULL if not available
   */
  public function getPatternAfterTime(): ?string {
    return $this->patternAfterTime ?? NULL;
  }

  /**
   * Parser for the PatternAfterTime as in '... after {13:00}'.
   *
   * @param array $time_pieces
   *   The exploded time string to parse.
   */
  protected function parsePatternAfterTime($time_pieces = []): void {
    // Time comes after the word 'after'.
    $time = $this->parseRetriever('after', $time_pieces, 1);
    $this->setPatternAfterTime($time);
  }

  /**
   * Setter for PatternDay.
   *
   * @param mixed $possible_day
   *   The integer day to advance to.
   *
   * @return CronManager
   *   For chaining.
   */
  private function setPatternDay(mixed $possible_day = ''): CronManager {
    // Make sure it is an integer.
    settype($possible_day, "integer");
    // Sanitize for valid $possible_day being 1-31.
    $month = $this->getPatternMonth();
    if (!empty($month)) {
      $max_days = self::MAX_DAYS_IN_MONTH[$month];
    }
    else {
      // There is no month, so go with a max of 31.
      // Can get odd behavior if the day specified for pattern like
      // 'on the 31st of every 1 month' is more than the number of days in
      // a the current month.
      $max_days = 31;
    }

    if (($possible_day < 1) || ($possible_day > $max_days)) {
      $possible_day = NULL;
    }
    $this->patternDay = $possible_day;
    return $this;
  }

  /**
   * Getter for PatternDay.
   *
   * @return ?string
   *   Returns the day if there is one, NULL if there is not.
   */
  public function getPatternDay() {
    return $this->patternDay ?? NULL;
  }

  /**
   * Find the day that comes after the word 'the'.
   *
   * As in 'the {4th} of july"
   *
   * @param array $time_pieces
   *   The exploded time string to parse.
   */
  protected function parsePatternDay(array $time_pieces = []) {
    // Day comes after the word 'the'.
    $day = $this->parseRetriever('the', $time_pieces, 1);
    $this->setPatternDay($day);
  }

  /**
   * Parser to find the needle in an array, and return the item offset from it.
   *
   * @param string $needle
   *   The thing in the array we are looking for.
   * @param array $haystack
   *   The array to search through for the needle within haystack.
   * @param int $offset
   *   How many items away from the needle to jump to get the returned item.
   *
   * @return ?string
   *   The item that resides at needle + offset.
   */
  public function parseRetriever(string $needle = '', array $haystack = [], int $offset = 0): ?string {
    // Locate the $needle.
    $needle_index = array_search($needle, $haystack);
    $target_index = $needle_index + $offset;
    if ((!is_null($needle_index)) && ($needle_index !== FALSE) && (!empty($haystack[$target_index]))) {
      return $haystack[$target_index];
    }
    return NULL;
  }

  /**
   * Gets the time since the last time the specified cron function ran from db.
   *
   * @return CronManager
   *   For chaining.
   */
  protected function queryGetCronTime(): CronManager {
    $batch_operation_name = $this->getBatchOperationName();
    if (!empty($batch_operation_name)) {
      // Get the time when $cron_function_name last ran.
      $connection = $this->batchOperations->getDatabaseConnection();
      $n_time_last_ran = $connection->query('SELECT ran_date FROM {codit_batch_operations_cron} WHERE batch_operation_name = :name', [':name' => $batch_operation_name])->fetchField();
      // Check to see if we have a time.  If not, it hasn't ever run.
      if (($n_time_last_ran == 0) ||
          (empty($n_time_last_ran)) ||
          (!is_numeric($n_time_last_ran)) ||
          ((int) $n_time_last_ran != $n_time_last_ran)) {
        // Means this has never run or seems to be not a valid time, set Flag.
        $this->firstRun = TRUE;
        // Initialize the time as now. This is an assumption, but we need
        // to start somewhere.
        $n_time_last_ran = NULL;
      }
      $this->setCronLastRan($n_time_last_ran);
    }
    return $this;
  }

  /**
   * Sets the current time in the table as the last time the function executed.
   *
   * @return CronManager
   *   For chaining.
   */
  public function querySetCronTime(): CronManager {
    $batch_operation_name = $this->getBatchOperationName();
    try {
      $connection = $this->batchOperations->getDatabaseConnection();
      $db_write_status = $connection->merge('codit_batch_operations_cron')
        ->key('batch_operation_name', $batch_operation_name)
        ->insertFields([
          'batch_operation_name' => $batch_operation_name,
          'ran_date' => time(),
        ])
        ->updateFields(['ran_date' => time()])
        ->execute();
      // Provide feedback on what happened.
      $msg_vars = [
        '@name' => $batch_operation_name,
      ];

      switch ($db_write_status) {
        case Merge::STATUS_INSERT:
          $this->batchOperations->logger->info('BatchOperation @name executed for the first time.', $msg_vars);
          $this->setCronLastRan(time());
          break;

        case Merge::STATUS_UPDATE:
          $this->batchOperations->logger->info('BatchOperation @name executed.', $msg_vars);
          $this->setCronLastRan(time());
          break;

        default:
          $this->batchOperations->logger->warning('The run time recording for BatchOperation @name failed to record the time in the cron_time table.  This BatchOperation will execute on the next cron.', $msg_vars);
          throw new \Exception("The run time recording for cron function $batch_operation_name failed to record the time in the cron_time table.  This cron function will execute on the next cron.");
      }
    }
    catch (\Exception $e) {
      // Something went wrong somewhere in the transaction.
      // Log the exception.
      $msg_vars['@error'] = $e->getMessage();
      $this->batchOperations->logger->exception('The cron run recording for BatchOperation @name failed catastrophically. Undoing transaction', $msg_vars);
      throw new \Exception("The run time recording for cron {$batch_operation_name} failed badly. {$e->getMessage()}");
    }
    return $this;
  }

  /**
   * Getter for the CronLastRan, reading it from the DB if not available.
   *
   * @return ?int
   *   Unix / epoch time stamp for of when the cron last ran.
   */
  public function getCronLastRan(): ?int {
    // Check to see if we already have the time.
    if (empty($this->cronLastRan)) {
      // We don't know when it ran yet, so query the DB.
      $this->queryGetCronTime();
    }
    return $this->cronLastRan ?? NULL;
  }

  /**
   * Setter for CronLastRan. Uses current time if no time is provided.
   *
   * @param string $when
   *   The last time the cron ran.
   *
   * @return CronManager
   *   For chaining.
   */
  protected function setCronLastRan($when = ''): CronManager {
    if (empty($when)) {
      // We need to establish this starting point in the DB.
      $this->querySetCronTime();
    }
    $when = (empty($when)) ? time() : $when;
    $this->cronLastRan = $when;
    return $this;
  }

  /**
   * Calculates when the next full execution should be allowed.
   *
   * @return int
   *   Unix epoch time of the next threshold for full execution of cron func.
   */
  public function getNextRun() {
    // Case Race, first to evaluate TRUE wins.  Start with most complex and
    // move toward simpler to avoid false positives.
    switch (TRUE) {
      // Has day, month, afterTime [on the 4th of July after 14:00].
      case ($this->getPatternDay()) && ($this->getPatternMonth()) && ($this->getPatternAfterTime()):
        $this->rollForwardToMonth();
        $this->rollForwardToDay();
        $this->addAfterTime();
        break;

      // Has day, month [on the 4th of July].
      case ($this->getPatternDay()) && ($this->getPatternMonth()):
        $this->rollForwardToMonth();
        $this->rollForwardToDay();
        break;

      // Skip, units, day,afterTime [on the 15th of every 4 months after 16:00].
      case ($this->getPatternSkipQty()) && ($this->getPatternSkipUnit()) && ($this->getPatternDay()) && ($this->getPatternAfterTime()):
        $this->addTimeFromUnits();
        $this->rollbackMonthToFirst();
        $this->rollForwardToDay();
        $this->addAfterTime();
        break;

      // Has skip, units, day ['on the 6th of every 1 month']].
      case ($this->getPatternSkipQty()) && ($this->getPatternSkipUnit()) && ($this->getPatternDay()):
        $this->addTimeFromUnits();
        $this->rollbackMonthToFirst();
        $this->rollForwardToDay();
        break;

      // Has skip, units, afterTime  ['every 2 months after 14:00']].
      case ($this->getPatternSkipQty()) && ($this->getPatternSkipUnit()) && ($this->getPatternAfterTime()):
        $this->addTimeFromUnits();
        $this->addAfterTime();
        break;

      // Has skip, units ['every 2 months']].
      case ($this->getPatternSkipQty()) && ($this->getPatternSkipUnit()):
        $this->addTimeFromUnits();
        $this->needsForceRunToInstantiate = TRUE;
        break;

      // It made it this far so it has either:
      // - only has skip and no units - A malformed time string.
      // - only has units and no skip - A malformed time string.
      default:
        return NULL;
    }

    $this->adjustForPastPresent();

    return $this->getCronShouldRunAfter();
  }

  /**
   * Roll forward getCronShouldRunAfter to the given PatternMonth.
   *
   * As in 'on the 4th of {July}'.
   *
   * @return CronManager
   *   For chaining.
   */
  protected function rollForwardToMonth(): CronManager {
    // Make sure we have a $PatternMonth.
    if (!empty($this->getPatternMonth())) {
      $time_to_build_upon = $this->getCronShouldRunAfter();
      $time_string = $this->getPatternMonth();
      // Roll To that month but uncertain as to whether it went back or forward.
      $new_time = (strtotime("{$time_string} 1st", $time_to_build_upon));
      // The month pick always lands us in the the given year, so that may be
      // past or future.  However we can't assess which is right or wrong until
      // After all the other adjustments take place.
      $this->setCronShouldRunAfter($new_time);
    }
    return $this;
  }

  /**
   * Adjust planned time for future if needed.
   *
   * @return CronManager
   *   For chaining.
   */
  protected function adjustForPastPresent(): CronManager {
    // Get the starting time to add upon.
    $time_to_build_upon = $this->getCronShouldRunAfter();
    $time_last_ran = $this->getCronLastRan();
    if (($time_to_build_upon < $time_last_ran) && (!empty($this->getPatternMonth()))) {
      // The time is before the time of last run, and we have specific month so
      // now we can look for the next opportunity in the coming year.
      // We are in the past, so add a year.
      $time_string = '+1 year';
      $new_time = (strtotime($time_string, $time_to_build_upon));
      $this->setCronShouldRunAfter($new_time);
    }

    return $this;
  }

  /**
   * Adds {SkipQty}{SkipUnits} to the base time.  A rollForward.
   *
   * @return CronManager
   *   For chaining.
   */
  protected function addTimeFromUnits(): CronManager {
    // Check to see if I have SkipQty and SkipUnits.
    if (!empty($this->getPatternSkipQty()) && !empty($this->getPatternSkipUnit())) {
      // Get the starting time to add upon.
      $time_to_build_upon = $this->getCronShouldRunAfter();
      $time_string = "+{$this->getPatternSkipQty()} {$this->getPatternSkipUnit()}";
      $new_time = (strtotime($time_string, $time_to_build_upon));
      $debug = [
        'old' => date('Y-m-d', $time_to_build_upon),
        'new' => date('Y-m-d', $new_time),
      ];

      $this->setCronShouldRunAfter($new_time);
    }
    return $this;
  }

  /**
   * Round down the month to day 1 @ 00:00 for the month CronShouldRunAfter.
   *
   * @return CronManager
   *   For chaining.
   */
  protected function rollbackMonthToFirst(): CronManager {
    // Get the starting time to build upon.
    $time_to_build_upon = $this->getCronShouldRunAfter();
    $time_string = 'first day of';
    // Roll back to the first day of the month that $time_to_build_upon is in.
    $new_time = (strtotime($time_string, $time_to_build_upon));
    $this->setCronShouldRunAfter($new_time);
    $this->rollbackToMidnight();
    return $this;
  }

  /**
   * For the day that CronShouldRunAfter is on, set the time to midnight 00:00.
   *
   * @return CronManager
   *   For chaining.
   */
  protected function rollbackToMidnight(): CronManager {
    // Get the starting time to build upon.
    $time_to_build_upon = $this->getCronShouldRunAfter();
    // Now roll back to midnight of that day.
    $time_string = 'midnight';
    $new_time = (strtotime($time_string, $time_to_build_upon));
    $this->setCronShouldRunAfter($new_time);
    return $this;
  }

  /**
   * Adds the PatternAfterTime to the current CronShouldRunAfter.
   *
   * @return CronManager
   *   For chaining.
   */
  protected function addAfterTime(): CronManager {
    // Check to see if an PatternAfterTime exists.
    if (!empty($this->getPatternAfterTime())) {
      // Will roll back time to midnight first.
      $this->rollbackToMidnight();
      $time_to_build_upon = $this->getCronShouldRunAfter();
      $time_string = $this->getPatternAfterTime();
      $new_time = (strtotime($time_string, $time_to_build_upon));
      $this->setCronShouldRunAfter($new_time);
    }
    return $this;
  }

  /**
   * Takes the month that CronShouldRunAfter is in and goes to that PatternDay.
   *
   * As in 'on the {15th} of July'.
   *
   * @return CronManager
   *   For chaining.
   */
  protected function rollForwardToDay(): CronManager {
    // Check if I have the Pattern.
    if (!empty($this->getPatternDay())) {
      // Rollback the month we are in, to the first of the month.
      $this->rollbackMonthToFirst();
      $time_to_build_upon = $this->getCronShouldRunAfter();
      // Roll forward to the Day in the month that the CronShouldRunAfter is in.
      // Need to subtract 1 day since months don't start at 0.
      $time_string = $this->getPatternDay() - 1 . ' days';
      $new_time = (strtotime($time_string, $time_to_build_upon));
      $this->setCronShouldRunAfter($new_time);
    }
    return $this;
  }

  /**
   * Sets the value of when the cron should be run next.
   *
   * @param ?int $cron_future_time
   *   An unix epoch time integer.
   *
   * @return CronManager
   *   For chaining.
   */
  protected function setCronShouldRunAfter(?int $cron_future_time = 0): CronManager {
    // This needs to be safe in that accidentally passing it nothing, should
    // not wipe out any times previously set.  There is no reason for 0 time.
    if (!empty($cron_future_time)) {
      $this->cronShouldRunAfter = $cron_future_time;
    }
    return $this;
  }

  /**
   * Gets the unix time for when this cron function is clear to run.
   *
   * @return int
   *   Unix time epoch for the threshold when cron function should fully run.
   */
  public function getCronShouldRunAfter(): int {
    if (empty($this->cronShouldRunAfter)) {
      // Can't have null starting place, so try to get when it last ran.
      $this->setCronShouldRunAfter($this->getCronLastRan());
      // There is still a chance this could be empty, so back it up with the
      // the current time if it is.
      if (empty($this->cronShouldRunAfter)) {
        $this->setCronShouldRunAfter(time());
      }
    }
    return $this->cronShouldRunAfter;
  }

  /**
   * Returns a human readable array of timing information.
   *
   * This is just for debugging.
   *
   * @return array
   *   A human readable threshold for when the cron last recorded a run.
   */
  public function getDebug() {
    return [
      'Time pattern' => $this->getPatternSubmitted(),
      'needsForceRunToInstantiate' => ($this->needsForceRunToInstantiate) ? 'Yes' : 'No',
      'Current time' => date('m/d/Y - H:i', time()),
      'Last recorded run' => date('m/d/Y - H:i', $this->getCronLastRan()),
      'Runs after' => date('m/d/Y - H:i', $this->getCronShouldRunAfter()),
      'FirstRun' => ($this->firstRun) ? 'Yes' : 'No',
      'ShouldItRun' => ($this->evalShouldItRun()) ? 'Yes' : 'No',
    ];

  }

  /**
   * Evaluates whether this cron function should fully execute.
   *
   * @return bool
   *   Indicating whether the current time meets the criteria to fully execute
   *   this cron function.
   */
  public function evalShouldItRun() {
    $next_run = $this->getCronShouldRunAfter();
    $now = time();
    if ($this->firstRun && $this->needsForceRunToInstantiate) {
      // This is a special case where an 'every' string needs to run initially
      // in order to establish its baseline.
      return TRUE;
    }
    // Are we past the threshold right now?
    return ($next_run - $now > 0) ? FALSE : TRUE;
  }

}
