<?php

namespace Drupal\codit_batch_operations;

use Drupal\node\NodeInterface;
use Drupal\node\NodeStorageInterface;

/**
 * Common node related methods for optional use during BatchOperations.
 */
trait BatchOperationsNodeTrait {

  /**
   * Get the node storage.
   *
   * @return \Drupal\node\NodeStorageInterface
   *   Node storage.
   */
  public function getNodeStorage(): NodeStorageInterface {
    return $this->entityTypeManager->getStorage('node');
  }

  /**
   * Load the latest revision of a node.
   *
   * @param int $nid
   *   The node ID.
   *
   * @return \Drupal\node\NodeInterface
   *   The latest revision of that node.
   */
  public function getNodeLatestRevision(int $nid): NodeInterface {
    $node_storage = $this->getNodeStorage();
    // @todo Resolve deprecation.
    return $node_storage->loadRevision($node_storage->getLatestRevisionId($nid));
  }

  /**
   * Load the default revision of a node.
   *
   * If never published, the default revision is the most recent draft.  When
   * published, the gets the most recent published revision.
   *
   * @param int $nid
   *   The node ID.
   *
   * @return \Drupal\node\NodeInterface
   *   The latest revision of that node.
   */
  public function getNodeDefaultRevision(int $nid): NodeInterface {
    return $this->getNodeStorage()->load($nid);
  }

  /**
   * Load all revisions of a node.
   *
   * @param int $nid
   *   The node ID.
   *
   * @return \Drupal\node\NodeInterface[]
   *   All revisions of that node.
   */
  public function getNodeAllRevisions(int $nid): array {
    $node_storage = $this->getNodeStorage();
    $node = $node_storage->load($nid);
    $vids = $node_storage->revisionIds($node);
    return $node_storage->loadMultipleRevisions($vids);
  }

  /**
   * Load all from the default and any forward revisions beyond that.
   *
   * @param int $nid
   *   The node ID.
   *
   * @return \Drupal\node\NodeInterface[]
   *   The default and all forward revisions of that node.
   */
  public function getNodeDefaultAndForwardRevisions(int $nid): array {
    $node_storage = $this->getNodeStorage();
    $node = $node_storage->load($nid);
    $rids = $node_storage->revisionIds($node);
    $default_node = $this->getNodeDefaultRevision($nid);
    $default_rid = $default_node->getRevisionId();
    $default_and_forward_rids = [];
    foreach ($rids as $rid) {
      if ($rid >= $default_rid) {
        // This must be either default or forward so add it to list.
        $default_and_forward_rids[] = $rid;
      }
    }
    return $node_storage->loadMultipleRevisions($default_and_forward_rids);
  }

  /**
   * Get an array of node ids for batch processing.
   *
   * @param string $node_bundle
   *   The bundle name of the nodes to lookup.
   * @param bool $published_only
   *   TRUE if you need only published nodes.
   *
   * @return array
   *   An array of nids for for the requested bundle, keyed by 'NID'.
   */
  public function getNidsOfType($node_bundle, $published_only = FALSE): array {
    $query = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('type', $node_bundle)
      ->accessCheck(FALSE);
    if ($published_only) {
      $query->condition('status', 1);
    }
    $nids = $query->execute();
    // Use the node ids as the keys, regardless of bundle type.
    $node_ids = array_combine(array_values($nids), array_values($nids));
    return $node_ids;
  }

  /**
   * Saves a node revision with log messaging.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to serialize.
   * @param string $message
   *   The log message for the new revision.
   * @param bool $new
   *   Whether the revision should be created or updated.
   *
   * @return int
   *   Either SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   */
  public function saveNodeRevision(NodeInterface $node, $message = '', $new = TRUE): int {
    $moderation_state = $node->get('moderation_state')->value;
    $node->setNewRevision($new);
    $node->setSyncing(TRUE);
    $node->setValidationRequired(FALSE);
    $node->enforceIsNew(FALSE);
    // New revisions deserve special treatment.
    if ($new) {
      $node->setChangedTime(time());
      $node->setRevisionCreationTime(time());
      $uid = $this->getUser();
    }
    else {
      $uid = $node->getRevisionUserId();
      // Append new log message to previous log message.
      $prefix = !empty($message) ? $node->getRevisionLogMessage() . ' - ' : '';
      $message = $prefix . $message;
    }
    $node->setRevisionUserId($uid);
    $revision_time = $node->getRevisionCreationTime();
    // Incrementing by a nano second to bypass Drupal core logic
    // that will update the "changed" value to request time if
    // the value is not different from the original value.
    $revision_time++;
    $node->setRevisionCreationTime($revision_time);
    $node->setRevisionLogMessage($message);
    $node->set('moderation_state', $moderation_state);

    return $node->save();
  }

  /**
   * Saves a node revision with no new revision or log.
   *
   * @param \Drupal\node\NodeInterface $revision
   *   The node to serialize.
   *
   * @return int
   *   Either SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   */
  public function saveNodeExistingRevisionWithoutLog(NodeInterface $revision): int {
    $revision->setNewRevision(FALSE);
    $revision->enforceIsNew(FALSE);
    $revision->setSyncing(TRUE);
    $revision->setValidationRequired(FALSE);
    $revision_time = $revision->getRevisionCreationTime();
    // Incrementing by a nano second to bypass Drupal core logic
    // that will update the "changed" value to request time if
    // the value is not different from the original value.
    $revision_time++;
    $revision->setRevisionCreationTime($revision_time);
    $revision->setChangedTime($revision_time);
    return $revision->save();
  }

}
