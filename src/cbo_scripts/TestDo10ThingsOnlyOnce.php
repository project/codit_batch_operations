<?php

namespace Drupal\codit_batch_operations\cbo_scripts;

use Drupal\codit_batch_operations\BatchOperations;
use Drupal\codit_batch_operations\BatchScriptInterface;

/**
 * A test & example BatchOperation to show something that can only run once.
 */
class TestDo10ThingsOnlyOnce extends BatchOperations implements BatchScriptInterface {

  /**
   * {@inheritdoc}
   */
  public function getTitle(): string {
    return 'Do 10 things but not actually do anything other than log things. And only allow them once.';
  }

  /**
   * {@inheritdoc}
   */
  public function getItemType(): string {
    return 'launch_step';
  }

  /**
   * {@inheritdoc}
   */
  public function getAllowOnlyOneCompleteRun(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    $description = <<<ENDHERE
    This is intended to be an example of a BatchOperation that can only run completely once.  It has an error
    built in so that it can test that it can be run with failures multiple times, but only completed once.
    ENDHERE;
    return $description;
  }

  /**
   * {@inheritdoc}
   */
  public function getCompletedMessage(): string {
    // This message can include the tokens '@completed' and '@total'.
    return 'The launce sequence completed @completed out of @total steps.';
  }

  /**
   * {@inheritdoc}
   */
  public function gatherItemsToProcess(): array {
    // Do whatever you need to here to put together the list of items
    // to be processed. Can be a keyed array like
    // [key1 => item1, key2 => item2 ...]
    // or a flat array [item1, item2, item3 ...].
    // 'item' can be something simple like a node id for processOne() to load,
    // or could be a loaded entity to act on.
    $launch_rocket_steps = [
      '1' => 'Move rocket to launch pad.',
      '2' => 'Connect hoses.',
      '3' => 'Fill primary fuel tanks.',
      '4' => 'Pressure check on primary fuel tanks.',
      '5' => 'Fill secondary fuel tanks.',
      '6' => 'Pressure check on secondary fuel tanks.',
      '7' => 'Insert astronauts.',
      '8' => 'Check communications.',
      '9' => 'Final system checks',
      '10' => 'Blast off.',
    ];
    return $launch_rocket_steps;
  }

  /**
   * {@inheritdoc}
   */
  public function processOne(string $key, mixed $item, array &$sandbox): string {
    // Do some things in here, then return a message about what was done.
    // If you return a non-empty message, it will get logged in the BatchOpLog.
    // If you were doing a big process and wanted to add to the log or errors,
    // you can log specifically as you go.
    if ($key === 'launch_step_4') {
      $this->batchOpLog->appendLog('Pressure check failed We have a leak.');
      // Create an error by calling fictional function.
      // @phpstan-ignore-next-line
      run_for_your_lives_there_is_a_leak();
    }

    return "Step {$item} completed.";
  }

}

// @codingStandardsIgnoreStart
// Example of how to run this batch from a hook_update_n()
/**
 * Run a BatchOperation example with that launches a rocket one time.
 */
// function my_module_update_9012(&$sandbox) {
//  $script = \Drupal::classResolver('\Drupal\codit_batch_operations\cbo_scripts\TestDo10ThingsOnlyOnce');
//  return $script->run($sandbox, 'hook_update');
// }
//
//
// Run with drush:
// drush codit-batch-operations:run TestDo10ThingsOnlyOnce
// @codingStandardsIgnoreEnd
