<?php

namespace Drupal\codit_batch_operations\cbo_scripts;

use Drupal\codit_batch_operations\BatchOperations;
use Drupal\codit_batch_operations\BatchScriptInterface;

/**
 * A test and example Batch operation script to show processing and logging.
 */
class TestDo10000Things extends BatchOperations implements BatchScriptInterface {

  /**
   * The unix timestamp with microtime of the previous processOne().
   *
   * @var float
   */
  protected float $lastRunTime;

  /**
   * {@inheritdoc}
   */
  public function getTitle():string {
    return 'Do 10,000 things but not actually do anything other than log and time things.';
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription():string {
    $description = <<<ENDHERE
    This BatchOperation does a whole lot of nothing.  It does 10,000 rounds of
    nothing, and so it is good for testing speed and log size.
    ENDHERE;

    return $description;
  }

  /**
   * {@inheritdoc}
   */
  public function getCompletedMessage(): string {
    // This message can include the tokens '@completed' and '@total'.
    return '10,0000 log entries were made and I am tired.';
  }

  /**
   * {@inheritdoc}
   */
  public function gatherItemsToProcess(): array {
    // Do whatever you need to here in order to put together the list of items
    // to be processed. Can be a keyed array like
    // [key1 => item1, key2 => item2 ...]
    // or a flat array [item1, item2, item3 ...].
    // 'item' can be something simple like a node id for processOne() to load,
    // or could be a loaded entity act on.
    $items = array_fill(1, 10000, 'Twiddle thumbs fast.');
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function processOne(string $key, mixed $item, array &$sandbox): string {
    // Checking the times to track the speed of writing log items as the
    // log gets bigger.
    $now = microtime(TRUE);
    $time_message = '';
    if (!empty($this->lastRunTime)) {
      $last_recorded_time = $this->lastRunTime;
      $elapsed = round(($now - $last_recorded_time), 4);
      $time_message = "- Logging took {$elapsed} seconds.";
    }
    $this->lastRunTime = microtime(TRUE);

    return "{$item}  {$time_message}";
  }

}

// @codingStandardsIgnoreStart
// Example of how to run this batch from a hook_update_n()
/**
 * Run a script example with 10,000 maniacs.
 */
// function my_module_update_9012(&$sandbox) {
//  $script = \Drupal::classResolver('\Drupal\codit_batch_operations\cbo_scripts\TestDo10000Things');
//  return $script->run($sandbox, 'hook_update');
// }
//
//
// Run with drush:
// drush codit-batch-operations:run TestDo10000Things
// @codingStandardsIgnoreEnd
