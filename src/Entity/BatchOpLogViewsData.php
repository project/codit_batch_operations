<?php

declare(strict_types=1);

namespace Drupal\codit_batch_operations\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for CMDocument entities.
 */
class BatchOpLogViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();
    // @todo This entire class might not be needed.
    return $data;
  }

}
