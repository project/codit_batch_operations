<?php

namespace Drupal\codit_batch_operations\Drush\Commands;

use Drupal\codit_batch_operations\BatchOperationsFilesTrait;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\State\State;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Token;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Drush\Drush;
use Drush\Exceptions\CommandFailedException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush command file.
 */
final class CoditBatchOperationsCommands extends DrushCommands implements ContainerInjectionInterface {

  use BatchOperationsFilesTrait;
  use StringTranslationTrait;

  /**
   * The configuration for codit_batch_operations.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $cboConfig;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;


  /**
   * Module handler interface.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  public $moduleHandler;

  /**
   * The state api service.
   *
   * @var \Drupal\Core\State\State
   */
  public $state;

  /**
   * Constructs a ContentModelDocumentationCommands object.
   */
  public function __construct(
    private readonly Token $token,
    ConfigFactoryInterface $config_factory,
    ModuleHandlerInterface $module_handler,
    State $state,
  ) {
    parent::__construct();
    $this->configFactory = $config_factory;
    $this->cboConfig = $this->configFactory->getEditable('codit_batch_operations.settings');
    $this->moduleHandler = $module_handler;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('token'),
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('state')
    );
  }

  /**
   * Runs the BatchOperation script if it exists.
   *
   * @param string $script
   *   The name of the BatchOperation script to run.
   * @param array $options
   *   The array of option flags passed in.
   */
  #[CLI\Command(name: 'codit-batch-operations:run', aliases: ['batch-op-run'])]
  #[CLI\Argument(name: 'script', description: 'The Batch Operation script to run.')]
  #[CLI\Option(name: 'allow-skip', description: 'Allow skipping batch items if they error.')]
  #[CLI\Usage(name: 'drush codit-batch-operations:run TestDo10Things', description: 'Runs the script named TestDo10Things.')]
  public function run(string $script, $options = ['allow-skip' => FALSE]) {
    try {
      $scriptClass = $this->getNamespacedClassName($script);
      $this->checkScriptClass($scriptClass);
      $sandbox = [];
      // @phpstan-ignore-next-line
      $script = \Drupal::classResolver($scriptClass);

      do {
        $msg = $script->run($sandbox, 'drush', $options['allow-skip']);
        Drush::output()->writeln($msg);
      } while ($sandbox['#finished'] < 1);
      if ($sandbox['skipped_items']) {
        Drush::output()->writeln('Skipped items:');
        Drush::output()->writeln(print_r($sandbox['skipped_items'], TRUE));

      }
      if ($script->getLoggedErrors()) {
        Drush::output()->writeln('Errors:');
        Drush::output()->writeln(print_r($script->getLoggedErrors(), TRUE));
      }
      $msg = '';
    }
    catch (\Exception $e) {
      $vars = [
        '@error' => $e->getMessage(),
      ];
      $msg = dt("Codit Batch Operations:  @error", $vars);
    }

    return $msg;
  }

  /**
   * Lists all the BatchOperation scripts available.
   *
   * @param array $options
   *   The array of option flags passed in.
   */
  #[CLI\Command(name: 'codit-batch-operations:list', aliases: ['batch-op-list'])]
  #[CLI\Option(name: 'tests', description: 'Include the test Batch Operations.')]
  #[CLI\Usage(name: 'drush codit-batch-operations:list', description: 'Lists all the Batch Operations.')]
  #[CLI\Usage(name: 'drush codit-batch-operations:list --tests', description: 'Lists all the Batch Operations and Test Batch Operations.')]
  public function list($options = ['tests' => FALSE]) {
    try {
      $vars = [
        '@batch_name' => 'BatchOperations',
      ];
      if ($options['tests']) {
        $batch_operations = $this->getBatchOperations(TRUE);
        Drush::output()->writeln($this->t('Looking up @batch_name including tests.', $vars));
      }
      else {
        $batch_operations = $this->getBatchOperations(FALSE);
        Drush::output()->writeln($this->t('Looking up @batch_name.', $vars));
      }
      $vars['@count'] = count($batch_operations);
      Drush::output()->writeln($this->t('Found @count @batch_name.', $vars));

      foreach ($batch_operations as $batch_operation) {
        $scriptClass = $this->getNamespacedClassName($batch_operation);
        // @phpstan-ignore-next-line
        $script = \Drupal::classResolver($scriptClass);
        $title = $script->getTitle();
        Drush::output()->writeln("{$batch_operation} -> {$title}");
      }

      $msg = '';
    }
    catch (\Exception $e) {
      $vars = [
        '@error' => $e->getMessage(),
      ];
      $msg = dt("Codit Batch Operations:  @error", $vars);
    }

    return $msg;
  }

  /**
   * Find what Batch Operation is on record as running.
   *
   * @param array $options
   *   The array of option flags passed in.
   */
  #[CLI\Command(name: 'codit-batch-operations:running', aliases: ['batch-op-running'])]
  #[CLI\Option(name: 'reset', description: 'Resets the Batch Operation running state to show nothing is running.')]
  #[CLI\Usage(name: 'drush codit-batch-operations:running', description: 'Shows what Batch Operation was running.')]
  #[CLI\Usage(name: 'drush codit-batch-operations:running --reset', description: 'Shows what Batch Operation was running, and resets it to not be on record as running.')]
  public function running($options = ['reset' => FALSE]) {
    $running = $this->state->get('cbo_running_script', NULL);
    $vars = [
      '@operation' => 'Batch Operation',
      '@running' => $running,
    ];
    if (empty($running)) {
      Drush::output()->writeln($this->t('There are no @operations running.', $vars));
      return;
    }
    if ($options['reset']) {
      $this->state->set('cbo_running_script', NULL);
      Drush::output()->writeln($this->t('The @operation @running was listed as running, but has been reset.', $vars));
    }
    else {
      Drush::output()->writeln($this->t('The @operation @running is listed as running', $vars));
    }
  }

  /**
   * Check to see if the script exists and is the right interface.
   *
   * @param string $scriptClass
   *   The fully namespaced class name of the script.
   *
   * @throws \Drush\Exceptions\CommandFailedException
   *   If the script is not available or not the right interface.
   */
  protected function checkScriptClass($scriptClass): void {
    if (!class_exists($scriptClass, TRUE)) {
      throw new CommandFailedException("The class $scriptClass does not exist.");
    }

    $implementations = class_implements($scriptClass, TRUE);
    $required_interface = 'Drupal\codit_batch_operations\BatchScriptInterface';
    if (empty($implementations) || !in_array($required_interface, $implementations)) {
      // The class is not the right interface.
      throw new CommandFailedException("The class $scriptClass must implement $required_interface.");
    }
  }

}
