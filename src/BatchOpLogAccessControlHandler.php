<?php

namespace Drupal\codit_batch_operations;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the BatchOpLog entity.
 *
 * @see \Drupal\codit_batch_operations\Entity\BatchOpLog.
 */
class BatchOpLogAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\codit_batch_operations\Entity\BatchOpLogInterface $entity */
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view codit batch operations log');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete codit batch operations log');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    // Logs are never created directly by a user.
    return AccessResult::neutral();
  }

}
