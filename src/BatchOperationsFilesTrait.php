<?php

namespace Drupal\codit_batch_operations;

/**
 * Common files related methods that can be used in multiple classes.
 */
trait BatchOperationsFilesTrait {

  /**
   * Get the list of operations existing as scripts including test operations.
   *
   * @param bool $include_test_files
   *   TRUE if it should include test files in the list. FALSE (default).
   *
   * @return array
   *   A flat array of script filenames.
   */
  public function getBatchOperations(bool $include_test_files = FALSE): array {
    $scripts = [];
    $test_location = 'codit_batch_operations';
    $script_location = $this->cboConfig->get('script_location');
    if (empty($script_location)) {
      $error = $this->t('A local module script location has not been set, please set one. Defaulting to showing test BatchOperations.');
      // Set the default location to the use the test BatchOperations.
      $script_location = $test_location;
    }
    if ($this->moduleHandler->moduleExists($script_location)) {
      $path = $this->moduleHandler->getModule($script_location)->getPath();
      $path .= '/src/cbo_scripts/';

      $files = scandir($path);
      $files = ($files) ? $files : [];
      if ($include_test_files || ($script_location === $test_location)) {
        $test_file_path = $this->moduleHandler->getModule($test_location)->getPath();
        $test_file_path .= '/src/cbo_scripts/';
        $test_files = scandir($test_file_path);
        $test_files = self::filterNonTests($test_files);
        $files = array_merge($files, $test_files);
      }

      foreach ($files as $file) {
        if (pathinfo($file, PATHINFO_EXTENSION) === 'php') {
          $scripts[] = pathinfo($file, PATHINFO_FILENAME);
        }
      }
    }
    sort($scripts, SORT_NATURAL);

    return $scripts;
  }

  /**
   * Checks to see if the batch operation exists.
   *
   * @param string $batch_operation_name
   *   The filename of the BatchOperation Class.
   *
   * @return bool
   *   True if the class exists.  FALSE otherwise.
   */
  public function isBatchOperation(string $batch_operation_name): bool {
    $all_operations = $this->getBatchOperations(TRUE);
    if (!empty($batch_operation_name) && in_array($batch_operation_name, $all_operations)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Gets the BatchOperation namespaced class.
   *
   * @param string $batch_operation_name
   *   The class short name for the BatchOperation.
   *
   * @return string
   *   The full local namespaced class for the BatchOperation.
   *   example: "\Drupal\codit_batch_operations\cbo_scripts\TestDo10Things".
   */
  public function getNamespacedClassName(string $batch_operation_name): string {
    $script_location = $this->cboConfig->get('script_location');
    if (str_starts_with($batch_operation_name, 'Test')) {
      // This is a test script so look for it in codit_batch_operations.
      $class = "\Drupal\codit_batch_operations\cbo_scripts\\{$batch_operation_name}";
    }
    else {
      // This is an actual script so look for it in the designated area.
      $class = "\Drupal\\{$script_location}\cbo_scripts\\{$batch_operation_name}";
    }
    return $class;
  }

  /**
   * Loads the requested BatchOperation class.
   *
   * @param string $batch_operation_name
   *   The class name for the BatchOperation.
   *
   * @return \Drupal\codit_batch_operations\BatchScriptInterface
   *   The loaded BatchOperation.
   */
  public function getBatchOperationClass(string $batch_operation_name): BatchScriptInterface {
    $class = $this->getNamespacedClassName($batch_operation_name);
    /** @var  \Drupal\codit_batch_operations\BatchScriptInterface $script*/
    // @phpstan-ignore-next-line (Can not use dependency injection for this.)
    $script = \Drupal::classResolver($class);
    return $script;
  }

  /**
   * Remove any BatchOperations that do not begin with Test.
   *
   * @param array $batch_operations
   *   A list of BatchOperations.
   *
   * @return array
   *   A list of BatchOperations with all non Tests removed.
   */
  public static function filterNonTests(array $batch_operations): array {
    foreach ($batch_operations as $key => $batch_operation) {
      if (!str_starts_with($batch_operation, 'Test')) {
        unset($batch_operations[$key]);
      }
    }
    return $batch_operations;
  }

}
