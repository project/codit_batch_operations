<?php

namespace Drupal\codit_batch_operations\Cron;

/**
 * Interface for the CronManager.
 *
 * Note this interface is incomplete.
 *
 * @todo There are more methods that should be revealed.
 */
interface CronManagerInterface {

  /**
   * Getter for the BatchOperationName.
   *
   * @return string
   *   The cron name of this cron function.
   */
  public function getBatchOperationName(): string;

  /**
   * Getter for submitted timing string pattern.
   *
   * @return string
   *   - string The timing string pattern submitted.
   *   - NULL if unavailable.
   */
  public function getPatternSubmitted() : string;

  /**
   * Getter for the SkipUnit from the timing string.
   *
   * @return ?string
   *   - string Timing unit like seconds, minutes, hours, days, months, years.
   *   - NULL if unavailable.
   */
  public function getPatternSkipUnit(): ?string;

}
