<?php

namespace Drupal\codit_batch_operations\cbo_scripts;

use Drupal\codit_batch_operations\BatchOperations;
use Drupal\codit_batch_operations\BatchScriptInterface;

/**
 * A test and example Batch operation script to show processing.
 */
class TestDo10Things extends BatchOperations implements BatchScriptInterface {

  /**
   * {@inheritdoc}
   */
  public function getTitle():string {
    return 'Do 10 things but not actually do anything other than log things.';
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription():string {
    $description = <<<ENDHERE
    This is intended as the simplest of examples for writing a BatchOperation
    It does not actually have any errors, but it does create some error text to
    show how if you find something odd in your processing, you can make note of
    it as an error for easy reference.
    ENDHERE;
    return $description;
  }

  /**
   * {@inheritdoc}
   */
  public function getCompletedMessage(): string {
    // This message can include the tokens '@completed' and '@total'.
    return 'Bullwinkle annoyed Rocky with @completed out of @total jokes.';
  }

  /**
   * {@inheritdoc}
   */
  public function gatherItemsToProcess(): array {
    // Do whatever you need to here to put together the list of items
    // to be processed. Can be a keyed array like
    // [key1 => item1, key2 => item2 ...]
    // or a flat array [item1, item2, item3 ...].
    // 'item' can be something simple like a node id for processOne() to load,
    // or could be a loaded entity to act on.
    $items = array_fill(1, 9, 'banana');
    $items[] = "Orange you glad I didn't say banana?";
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function processOne(string $key, mixed $item, array &$sandbox): string {
    // Do some things in here, then return a message about what was done.
    // If you return a non-empty message, it will get logged in the BatchOpLog.
    // If you were doing a big process and wanted to add to the log or errors,
    // you can log specifically as you go.
    if ($key === 'item_5') {
      $this->batchOpLog->appendLog('Again Bullwinkle?');
    }
    if ($key === 'item_8') {
      $this->batchOpLog->appendError("$key: Rocky grows impatient.");
    }

    return "Knock knock.  Who's there?  {$item}.";
  }

}

// @codingStandardsIgnoreStart
// Example of how to run this batch from a hook_update_n()
/**
 * Run a script example with knock-knock jokes and Rocky & Bullwinkle.
 */
// function my_module_update_9012(&$sandbox) {
//  $script = \Drupal::classResolver('\Drupal\codit_batch_operations\cbo_scripts\TestDo10Things');
//  return $script->run($sandbox, 'hook_update');
// }
//
//
// Run with drush:
// drush codit-batch-operations:run TestDo10Things
// @codingStandardsIgnoreEnd
