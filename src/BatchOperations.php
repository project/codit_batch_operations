<?php

namespace Drupal\codit_batch_operations;

use Drupal\codit_batch_operations\Cron\CronManager;
use Drupal\codit_batch_operations\Entity\BatchOpLog;
use Drupal\codit_batch_operations\Entity\BatchOpLogInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\Core\State\State;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class for Batch Operations.
 */
class BatchOperations implements ContainerInjectionInterface {

  use BatchOperationsFilesTrait;
  use BatchOperationsNodeTrait;
  use BatchOperationsVocabularyTrait;
  use DependencySerializationTrait;
  use StringTranslationTrait;

  public const RUNNING_KEY = 'cbo_running_script';

  /**
   * The account switcher service.
   *
   * @var \Drupal\Core\Session\AccountSwitcherInterface
   */
  protected $accountSwitcher;

  /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Database connector service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $databaseConnection;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  public $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public $entityTypeManager;

  /**
   * Module handler interface.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  public $moduleHandler;

  /**
   * The state api service.
   *
   * @var \Drupal\Core\State\State
   */
  public $state;

  /**
   * The name of what ran this.
   *
   * @var string
   */
  protected $executor;

  /**
   * The current BatchOpLog.
   *
   * @var \Drupal\codit_batch_operations\Entity\BatchOpLogInterface
   */
  protected $batchOpLog;

  /**
   * The configuration for codit_batch_operations.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $cboConfig;

  /**
   * The user id to use during the batch operation.
   *
   * @var int
   */
  protected int $user;


  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  public $logger;

  /**
   * The logger service.
   *
   * @var Drupal\Core\Messenger\MessengerInterface
   */
  public $messenger;

  /**
   * The total number of items to process.
   *
   * @var ?int
   */
  protected $totalItemCount = NULL;

  /**
   * Constructs the BatchOperations service.
   *
   * @param Drupal\Core\Session\AccountSwitcherInterface $account_switcher
   *   The account switcher service.
   * @param Drupal\Core\Cache\CacheBackendInterface $cache
   *   The backend cache service.
   * @param Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param Drupal\Core\Database\Connection $database_connection
   *   The database connection service.
   * @param Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Drupal messenger service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler for determining which modules are installed.
   * @param Drupal\Core\State\State $state
   *   The State API service.
   */
  final public function __construct(
    AccountSwitcherInterface $account_switcher,
    CacheBackendInterface $cache,
    ConfigFactoryInterface $config_factory,
    Connection $database_connection,
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeManagerInterface $entity_type_manager,
    LoggerChannelFactoryInterface $logger_factory,
    MessengerInterface $messenger,
    ModuleHandlerInterface $module_handler,
    State $state,
  ) {
    $this->accountSwitcher = $account_switcher;
    $this->cache = $cache;
    $this->configFactory = $config_factory;
    $this->databaseConnection = $database_connection;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger_factory->get('codit_batch_operations');
    $this->messenger = $messenger;
    $this->moduleHandler = $module_handler;
    $this->state = $state;
    $this->cboConfig = $this->configFactory->getEditable('codit_batch_operations.settings');
    $this->getBatchOpLog(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('account_switcher'),
      $container->get('cache.default'),
      $container->get('config.factory'),
      $container->get('database'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('module_handler'),
      $container->get('state')
    );
  }

  /**
   * Gets the short classname of the BatchOperation.
   *
   * @return string
   *   The class name only without the path.
   */
  public function getShortName(): string {
    return basename(str_replace('\\', '/', $this::class));
  }

  /**
   * Get the user id to use.
   *
   * @return int
   *   The user id from config or overridden.
   */
  public function getUser(): int {
    if (empty($this->user)) {
      // Not explicitly set, so get it from config.
      // If not set, default to user 1.
      $default_user_id = ($this->cboConfig->get('default_user')) ? $this->cboConfig->get('default_user') : 1;
      $this->setUser($default_user_id);
    }
    return $this->user;
  }

  /**
   * Set the user to use for batch operations and set it on the log too.
   *
   * @param int $user_id
   *   The user id of the user to use.
   */
  public function setUser(int $user_id): void {
    $this->user = $user_id;
    $this->getBatchOpLog();
    $this->batchOpLog->setOwnerId($user_id);
  }

  /**
   * Get the user storage.
   */
  public function getUserStorage(): UserStorageInterface {
    return $this->entityTypeManager->getStorage('user');
  }

  /**
   * Get the database connection.
   */
  public function getDatabaseConnection(): connection {
    return $this->databaseConnection;
  }

  /**
   * Switch to a different user for this operation or in the mid-operation.
   *
   * @param int|null $uid
   *   The UID of the account to switch.
   */
  public function switchUser(?int $uid = NULL): void {
    $use_default = empty($uid);
    $uid = $uid ?? $this->getUser();
    $user = $this->getUserStorage()->load($uid);
    $this->accountSwitcher->switchTo($user);
    if (!$use_default) {
      // Only log the user change if it is over riding the default.
      $this->batchOpLog->appendLog("Acting as {$user->getDisplayName()} [{$uid}]");
    }
    $this->batchOpLog->setOwnerId($uid);
  }

  /**
   * Initializes the basic sandbox values.
   *
   * @param array $sandbox
   *   Standard drupal $sandbox var to keep state in hook_update_N.
   *
   * @throws Drupal\Core\Utility\UpdateException
   *   If the counter callback can not be found.
   */
  public function initSandbox(array &$sandbox) {
    if (empty($sandbox['total'])) {
      // Sandbox has not been initiated.
      $script_name = get_class($this);
      $this->checkAndSetCanRun();
      $sandbox['multi_run_state_key'] = "cbo_{$script_name}";
      $sandbox['items_to_process'] = $this->getItemsToProcess();
      $sandbox['total'] = count($sandbox['items_to_process']);
      $sandbox['current'] = 0;
      $sandbox['BatchOpLog'] = $this->getBatchOpLog();
      $sandbox['skipped_items'] = [];
      $sandbox['this_batch'] = [];
      $sandbox['executor'] = $this->executor;
      if (empty($sandbox['total'])) {
        // There are no items to process, so bail out.
        return;
      }
      // @phpstan-ignore-next-line (known deprecated.method)
      $pre_msg = $this->preBatchMethod($sandbox);
      $pre_msg .= $this->preRun($sandbox);
      $this->batchOpLog->appendLog("preRun: {$pre_msg}");

      $this->batchOpLog->set('total_items', $sandbox['total']);
      $this->switchUser();
      $sandbox['multi_run_state_key'] = "cbo_{$script_name}";
      // This seems like the first run, see if there is already a state saved
      // from a previous attempt.
      $last_run_completed = $this->state->get($sandbox['multi_run_state_key']);
      if (!is_null($last_run_completed)) {
        // A state exists, so alter the 'current' and 'items_to_process'.
        $this->batchOpLog->set('last_item_processed', $last_run_completed);
        $this->batchOpLog->appendLog("A previous run detected that completed item {$last_run_completed}.");
        $sandbox['current'] = $last_run_completed + 1;
        // Remove the last successful run, and all that came before it.
        $sandbox['items_to_process'] = array_slice($sandbox['items_to_process'], $last_run_completed);
      }
    }
    $this->batchOpLog = $sandbox['BatchOpLog'];
    $sandbox['element'] = array_key_first($sandbox['items_to_process']);
  }

  /**
   * Method to run the whole batch operation.
   *
   * @param array $sandbox
   *   The sandbox by reference used to keep state across runs.
   * @param string $executor
   *   The thing that is executing the scripts (hook_update, deploy).
   * @param bool $allow_skip
   *   TRUE allows processing all items,  FALSE will stop on the first error.
   *
   * @return string
   *   Status and closing messages for each run.
   */
  public function run(array &$sandbox, string $executor, bool $allow_skip = FALSE):string {
    $this->setExecutor($executor);
    $this->initSandbox($sandbox);
    // @phpstan-ignore-next-line (Defined by extended class BatchOpLogInterface)
    $batch_items = array_slice($sandbox['items_to_process'], 0, $this->getBatchSize(), TRUE);
    foreach ($batch_items as $key => $item) {
      try {
        // @phpstan-ignore-next-line (Defined by extended class BatchOpLogInterface)
        $message = $this->processOne($key, $item, $sandbox);
        $this->batchOpLog->set('last_item_processed', $sandbox['current']);
        $this->batchOpLog->appendLog("{$key}: {$message}");
        $sandbox['this_batch'][] = $key;
        unset($sandbox['items_to_process'][$key]);
      }
      catch (\Throwable $th) {
        $error = $th->getMessage();
        $code = $th->getCode();
        $file = $th->getFile();
        $line = $th->getLine();
        $error_message = "Error: {$code} {$error} \n - Line {$line} of {$file}";
        $link = Link::fromTextAndUrl(t('View batch log'), $this->batchOpLog->getUrl());
        $vars = [
          '@completed' => $sandbox['current'],
          '@total' => $sandbox['total'],
          '@error' => $error_message,
          'link' => $link->toString(),
          '@key' => $key,
        ];
        $item_text = print_r($item, TRUE);

        if (!$allow_skip) {
          // We are not allowed to skip, so log error and get out.
          $this->logger->error("Error encountered while processing @key. => <pre>{$item_text}</pre></br> Completed @completed of @total.</br> @error", $vars);
          // @phpstan-ignore-next-line (known deprecated.method)
          $post_msg = $this->postBatchMethod($sandbox);
          $post_msg .= $this->postRun($sandbox);
          $this->batchOpLog->appendError("{$key} => {$item_text}")
            ->appendError($error_message)
            ->appendLog("postRun: {$post_msg}");

          // This is as far as we can go, so shutdown the tracking.
          $this->state->delete(self::RUNNING_KEY);
          if (empty($sandbox['executor'] === 'cron')) {
            // Not run by cron, so unravel user switching.
            $this->switchBackToOriginalUser();
          }

          $this->batchOpLog->save();
          throw new \Exception($error_message, $code);
        }
        else {
          // Skipping, so log as much as we can and move on.
          $sandbox['skipped_items'][$key] = $item;
          $this->batchOpLog->appendLog("{$key}: Skipped due to error.");
          $this->batchOpLog->appendError("Skipped {$key} => {$item_text}");
          $this->logger->error("Skipped due to error encountered while processing @key => <pre>{$item_text}</pre> </br>@error", $vars);
          // In order to skip it, we have to remove it.
          unset($sandbox['items_to_process'][$key]);
          $this->batchOpLog->appendError("{$key}: {$error_message}")
            ->save();
        }
      }
    }
    $this->addBatchSeparator();
    $this->batchOpLog->save();

    return $this->completeSandbox($sandbox);
  }

  /**
   * Batch API callback for running a single iteration of the Batch Operation.
   *
   * @param \Drupal\codit_batch_operations\BatchOperations $batch_operation
   *   The BatchOperation object passed from callback to callback.
   * @param bool $allow_skip
   *   Flag for if errors should be skipped, or fatal.
   * @param array $context
   *   A context array containing the sandbox for keeping state across runs.
   */
  public static function runBatchByUi(BatchOperations $batch_operation, bool $allow_skip, array &$context): void {
    // Juggle the sandbox because Batch API puts it inside $context.
    $sandbox = &$context['sandbox'];

    // Assign the current user.
    $current_user = \Drupal::currentUser();
    $uid = $current_user->id();
    $batch_operation->setUser($uid);
    try {
      // Run a single pass. The current function is re-called until finished=1.
      $batch_operation->run($sandbox, 'UI', $allow_skip);
    }
    catch (\Throwable $th) {
      $context['results']['error_msg'] = $th->getMessage();
      $context['results']['error_code'] = $th->getCode();

    }
    $batch_op_log = $batch_operation->getBatchOpLog();
    $context['message'] = "Processing {$sandbox['current']} of {$sandbox['total']}";
    $context['results']['batch_operation_name'] = $batch_operation->getShortName();
    $context['results']['max'] = $sandbox['total'];
    $context['results']['total'] = $sandbox['total'];
    $context['results']['count'] = $sandbox['current'];
    $context['results']['current'] = $sandbox['current'];
    // Carry existing errors forward, or check for a new one.
    $context['results']['errors'] = (!empty($context['results']['errors'])) ? TRUE : $batch_op_log->hasErrors();
    $context['results']['completed'] = $batch_op_log->getCompleted();
    if (isset($context['results']['error_msg'])) {
      // There is an error, end the batch now.
      $context['finished'] = 1;
      // Note: There is a discrepancy with setting the count.  The count
      // is correct on a first run failure. But, on a re-run failure it will
      // indicate it got one higher than it did.
      $context['results']['errors'] = TRUE;
    }
    else {
      $context['finished'] = $sandbox['#finished'];
    }

    if (!empty($batch_operation->batchOpLog) && empty($context['results']['log_link']) && $batch_op_log->canSave()) {
      $context['results']['log_link'] = new Link(t('View log'), $batch_op_log->getUrl());
      $context['results']['log_link'] = $context['results']['log_link']->toString();
    }
    else {
      $context['results']['log_link'] = '';
    }
  }

  /**
   * Method to run the whole batch operation from custom code.
   *
   * WARNING: This is not running the batch using the Batch API.  It is at risk
   * for php timeout so this may fail if it takes too long.  However, if it does
   * fail, it will pick up where it left off the next time it is called.  This
   * is not a preferred method of running a BatchOperation.
   *
   * @param string $executor
   *   The thing that is executing the scripts (Name of your function).
   * @param bool $allow_skip
   *   TRUE allows processing all items,  FALSE will stop on the first error.
   */
  public function runByCustomCode(string $executor, bool $allow_skip = TRUE): void {
    // Establish a sandbox to keep state across multiple runs.
    $sandbox = [];
    // Initiate the Finished state as not even started.
    $sandbox['#finished'] = 0;
    do {
      $this->run($sandbox, $executor, $allow_skip);
    } while ($sandbox['#finished'] < 1);
  }

  /**
   * The callback that gets called when the batch completes.
   *
   * @param bool $success
   *   TRUE if the batch completed.  FALSE otherwise.
   * @param array $results
   *   Storage for items assigned during the run.
   * @param mixed $operations
   *   A way to track uncompleted items.
   * @param string $elapsed
   *   The elapsed time with units.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect to go to the Batch operation page.
   */
  public static function finishedBatchByUi($success, $results, $operations, $elapsed): RedirectResponse {
    $msg_lead_in = ($results['count'] === $results['total']) ? t('Completed') : t('Incomplete');
    $msg_lead_in .= ' ';
    $msg_lead_in .= ($results['errors']) ? t('with errors') : t('without errors');
    $msg_vars = [
      '@count' => $results['count'],
      '@elapsed' => $elapsed,
      '@lead_in' => $msg_lead_in,
      '@link' => $results['log_link'],
      '@name' => $results['batch_operation_name'],
      '@max' => $results['max'],
    ];
    // When done, send us back to the BatchOperation page.
    $route_params = [
      'batch_operation_name' => $results['batch_operation_name'],
    ];

    if (!empty($results['error_code']) && $results['error_code'] === 423) {
      // Means it was only allowed to complete once, and it already has.
      $message = t('@name can only be run once, and has already run.', $msg_vars);
      \Drupal::messenger()->addError($message);
      return new RedirectResponse(Url::fromRoute('codit_batch_operations_ui.operation', $route_params)->toString());
    }

    if ($success && !$results['errors']) {
      $message = t('@name @lead_in. @count out of @max items processed in @elapsed: @link', $msg_vars);
      \Drupal::messenger()->addMessage($message);
    }
    else {
      $msg_vars['@error'] = $results['error_msg'] ?? t('none');
      $message = t('@name @lead_in. Something went wrong. Up to @count out of @max items processed in @elapsed. See log for details: @link  Details: @error', $msg_vars);
      \Drupal::messenger()->addWarning($message);
    }

    return new RedirectResponse(Url::fromRoute('codit_batch_operations_ui.operation', $route_params)->toString());
  }

  /**
   * Updates the counts and log if complete.
   *
   * @param array $sandbox
   *   Hook_update_n sandbox for keeping state.
   *
   * @return string
   *   String to be used as update hook messages.
   */
  public function completeSandbox(array &$sandbox) {
    // Determine when to stop batching.
    $sandbox['current'] = ($sandbox['total'] - count($sandbox['items_to_process']));
    $this->batchOpLog->set('last_item_processed', $sandbox['current']);
    // Save the 'current' value to state, to record a successful processOne().
    $this->state->set($sandbox['multi_run_state_key'], $sandbox['current']);
    $sandbox['#finished'] = (empty($sandbox['total'])) ? 1 : ($sandbox['current'] / $sandbox['total']);
    $vars = [
      '@completed' => $sandbox['current'],
      '@element' => implode(',', $sandbox['this_batch']),
      '@total' => $sandbox['total'],
      '@percentage' => empty($sandbox['total']) ? $this->t('none') : round(($sandbox['current'] / $sandbox['total']) * 100),
      '@skipped_count' => count($sandbox['skipped_items']),
    ];
    // The batch is done, reset the info from last batch.
    $sandbox['this_batch'] = [];

    $message = t('Processed @element. [@percentage%] [@completed/@total]', $vars);
    // Log the all finished notice.
    if ($sandbox['#finished'] === 1) {
      // @phpstan-ignore-next-line (Defined by extended class BatchOpLogInterface)
      $logged_message = new FormattableMarkup($this->getCompletedMessage(), $vars);
      $summary = new FormattableMarkup('Completed:  @completed/@total. Skipped:  @skipped_count/@total.', $vars);
      $message .= PHP_EOL;
      $message .= t('Process completed:') . " {$logged_message}" . PHP_EOL;
      // @phpstan-ignore-next-line (known deprecated.method)
      $post_msg = $this->postBatchMethod($sandbox);
      $post_msg .= $this->postRun($sandbox);
      $this->batchOpLog->setCompleted()
        ->appendLog($message)
        ->appendLog($summary)
        ->appendLog("postRun: {$post_msg}");
      $link = Link::fromTextAndUrl(t('View batch log'), $this->batchOpLog->getUrl());
      $vars['link'] = $link->toString();

      $this->logger->info("$logged_message </br> $summary", $vars);
      if (empty($sandbox['executor'] === 'cron')) {
        // Not run by cron, so unravel user switching.
        $this->switchBackToOriginalUser();
      }
      $this->batchOpLog->save();

      // Delete the state as it is no longer needed.
      $this->state->delete($sandbox['multi_run_state_key']);
      $this->state->delete(self::RUNNING_KEY);
    }
    if (($sandbox['executor'] === 'cron') && empty($sandbox['total'])) {
      // This was run by cron but had nothing to operate on.
      // Since there was no operation, delete the log.
      $this->batchOpLog->delete();
    }
    return $message;
  }

  /**
   * Unwraps layers of account switching to get us safely back to original user.
   */
  protected function switchBackToOriginalUser(): void {
    try {
      $this->accountSwitcher->switchBack();
      // AccountSwitcher::switchTo may have been called multiple times.
      // Calling switchBack undoes one call of switchTo, so keep calling it
      // until it throws an exception when there are no more layers to unwrap.
      $this->switchBackToOriginalUser();
    }
    catch (\Throwable $th) {
      // We have reached the end of the account switching.
      $message = $this->t('Account switched back to current user.');
      $this->batchOpLog->appendLog($message);
    }
  }

  /**
   * Set the executor for the operation and update the log.
   *
   * @param string $executor
   *   The thing executing the batch.
   */
  protected function setExecutor(string $executor): void {
    $this->executor = $executor;
    // We want the BatchOpLog to match.
    $this->batchOpLog->setExecutor($this->executor);
  }

  /**
   * Gets the name of any BatchOperation that might be currently running.
   *
   * @return ?string
   *   The class name of what is on record as running.  NULL if nothing running.
   */
  public function getWhatsRunning(): ?string {
    return $this->state->get(self::RUNNING_KEY, NULL);
  }

  /**
   * Checks if a BatchOperation script is already running or can't run again.
   *
   * @throws \RuntimeException
   *   Prevents a Batch Operation from running.
   */
  protected function checkAndSetCanRun(): void {
    $my_script_name = get_class($this);
    $script_running = $this->getWhatsRunning();
    $not_allowed_concurrent = ['drush', 'UI'];
    if ($this->getAllowOnlyOneCompleteRun()) {
      // This Batch operation should only run once.  See if it has already
      // completed.
      $batch_op_log = $this->getBatchOpLog();
      $most_recent_run_log = $batch_op_log->getMostRecentBatchOpLog($my_script_name);
      if (!empty($most_recent_run_log) && $most_recent_run_log->getCompleted()) {
        // The BatchOperation already ran to completion, it can not run again.
        // Prevent starting a BatchOpLog, because the process never started.
        $batch_op_log->setDoNotSave();
        $vars = [
          '@completed_date' => date('Y-m-d H:i:s', $most_recent_run_log->getLastUpdatedTime()),
        ];
        throw new \RuntimeException($this->t('This BatchOperation can only run once to completion. It was already completed on @completed_date', $vars), 423);
      }
    }
    if (($my_script_name === $script_running) && (in_array($this->executor, $not_allowed_concurrent))) {
      // The seems like a double run, which is bad. Run away screaming loudly.
      throw new \RuntimeException('This script is already running. Batch Operations scripts should never run at the same time. If you are certain no script is currently running, execute "drush codit-batch-operations:running --reset" or visit the settings page to remove the lock.', 409);
    }
    if (empty($script_running)) {
      // Nothing is recorded as running, so I am good to go.
      // Record me as the currently running script.
      $this->state->set(self::RUNNING_KEY, $my_script_name);
    }
    elseif (!empty($script_running) && (in_array($this->executor, $not_allowed_concurrent))) {
      // There is a script running and drush or the UI is running it, so bail.
      // Running by one of the hooks, means it is impossible for scripts to be
      // running concurrently.
      throw new \RuntimeException("The script $my_script_name is already running. Please wait until it completes. If you are certain no script is currently running, execute 'drush codit-batch-operations:running --reset' or visit the settings page to remove the lock.", 409);
    }
  }

  /**
   * Lookup a key in a map array and return the value from the map.
   *
   * @param string|null $lookup
   *   A map key to lookup. Do not lookup int as indexes can shift.
   * @param array $map
   *   An array containing string key value pairs. [lookup => value].
   * @param bool $strict
   *   TRUE = only want a value from the array, FALSE = want your lookup back.
   *
   * @return mixed
   *   Whatever the value associated with the key.
   */
  public function mapToValue(string|null $lookup, array $map, bool $strict = TRUE) : mixed {
    if (empty($lookup)) {
      if (isset($map['default'])) {
        // There is a default set, so use it.
        return $map['default'];
      }
      elseif ($strict) {
        return NULL;
      }
      else {
        return $lookup;
      }
    }
    if ($strict) {
      // Strict, so either it is there, or nothing.
      return $map[$lookup] ?? NULL;
    }
    else {
      // Not strict, so pass back what given it its not in the map.
      return $map[$lookup] ?? $lookup;
    }
  }

  /**
   * Modifies to items to process to make sure we have string keys.
   *
   * @return array
   *   The items to be processed.
   */
  protected function getItemsToProcess(): array {
    try {
      // @phpstan-ignore-next-line (Defined by extended class BatchOpLogInterface)
      $items = $this->gatherItemsToProcess();
    }
    catch (\Throwable $th) {
      // Something went wrong, surface the error.  Then set empty array.
      $vars = [
        '@class' => $this->getShortName(),
        '@error' => $th->getMessage(),
      ];
      $message = $this->t('Error in @class while gathering items. @error', $vars);
      $this->messenger->addError($message);
      $this->logger->error("{$message} </br> {$th->getTraceAsString()}");

      $items = [];
    }
    // Having flat arrays or numeric keyed array is problematic when it comes
    // to removing things from the array. As soon as you unset one, the array
    // becomes renumbered.  So we create string keys, from numeric values.
    // [35, 75, 20] becomes
    // ['item_35' => 35, 'item_75' => 75, 'item_20' => 20].
    if (isset($items['0'])) {
      // The leading key is 0.  Lets shift these to have them start at 1.
      $items = array_combine(range(1, count($items)), array_values($items));
    }
    $properly_keyed = array_combine(
      array_map([$this, 'stringify'], array_keys($items)),
      array_values($items));
    return $properly_keyed;
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalItemCount(): int {
    if (empty($this->totalItemCount)) {
      // Haven't been counted yet.
      $this->totalItemCount = count($this->getItemsToProcess());
    }
    return $this->totalItemCount;
  }

  /**
   * Callback function to convert an int to a string for keys.
   *
   * @param int|string $id
   *   A numeric id like a nid, tid or pid.
   *
   * @return string
   *   The id concatenated to the end of item_.
   */
  public function stringify($id) {
    return "{$this->getItemType()}_{$id}";
  }

  /**
   * Gets the BatchOpLog and sets it if it does not exist.
   *
   * @param bool $new
   *   A flag to force creating a new BatchOpLog.
   *
   * @return \Drupal\codit_batch_operations\Entity\BatchOpLogInterface
   *   The BatchOpLog.
   */
  public function &getBatchOpLog(bool $new = FALSE): BatchOpLogInterface {
    if (empty($this->batchOpLog) || $new) {
      // It doesn't exist so instantiate it.
      $this->batchOpLog = BatchOpLog::create([
        'name' => get_class($this),
        'user_id' => $this->getUser(),
        'executor' => $this->executor ?? 'not-set',
        'created' => time(),
        'completed' => FALSE,
      ]);
    }
    return $this->batchOpLog;
  }

  /**
   * Get the errors stored in the BatchOpLog.
   *
   * @return string
   *   The log record coming from the BatchOpLog entity.
   */
  public function getLoggedErrors() {
    return $this->batchOpLog->getErrors();
  }

  /**
   * Adds a separator to the log between batches.
   */
  protected function addBatchSeparator(): void {
    // @phpstan-ignore-next-line (Defined by extended class BatchOpLogInterface)
    if ($this->getBatchSize() > 1) {
      // We are using batches, so let's separate them in the log.
      $this->batchOpLog->appendLog('----- batch -----');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getItemType(): string {
    return 'item';
  }

  /**
   * Deprecated backward compatible version of preRun().
   *
   * @deprecated in codit_batch_operations:1.0.1 and is removed from codit_batch_operations:2.0.0 Use preRun() instead.
   * @see https://www.drupal.org/project/codit_batch_operations/issues/3463174
   */
  public function preBatchMethod(&$sandbox): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function preRun(&$sandbox): string {
    // This instance of preRun intentionally does nothing.
    // It is meant to be optionally overridden by the custom batch operation
    // class.
    return $this->t('Did nothing.');
  }

  /**
   * Deprecated backward compatible version of postRun().
   *
   * @deprecated in codit_batch_operations:1.0.1 and is removed from codit_batch_operations:2.0.0 Use postRun() instead.
   * @see https://www.drupal.org/project/codit_batch_operations/issues/3463174
   */
  public function postBatchMethod(&$sandbox): string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function postRun(&$sandbox): string {
    // This instance of postBatchMethod intentionally does nothing.
    // It is meant to be optionally overridden by the custom batch operation
    // class.
    return $this->t('Did nothing.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCronTiming(): string | array {
    return '';
  }

  /**
   * Get the next upcoming date & time after which the BatchOperation will run.
   *
   * @return string
   *   The string including date & time, and the cron timing that caused it.
   */
  public function getNextCronShouldRun(): string {
    $cron_timings = (array) $this->getCronTiming();
    $future_dates = [];
    foreach ($cron_timings as $cron_timing) {
      $cron_manager = new CronManager($this->getShortName(), $cron_timing, $this);
      $future_dates[$cron_timing] = $cron_manager->getCronShouldRunAfter();
    }
    // Figure out which timing is the nearest.
    asort($future_dates, SORT_NUMERIC);
    $date = date('m/d/Y - H:i', reset($future_dates));
    $cron_timing = array_key_first($future_dates);
    $via = $this->t('due to');
    return "{$date} {$via} '{$cron_timing}'";
  }

  /**
   * {@inheritdoc}
   */
  public function runOnlyOnce(): bool {
    // Sets the default to be overridden in the BatchOperation.
    return FALSE;
  }

  /**
   * Gets a list of all non-test BatchOperations that have cron times defined.
   *
   * @param bool $include_test_files
   *   TRUE if it should include test files in the list. FALSE (default).
   *
   * @return array
   *   An array containing elements with 'BatchOperation class' => [cron times].
   */
  public function getBatchOperationsWithCron(bool $include_test_files = FALSE): array {
    $batch_operations_with_cron = [];
    $cache_id = 'codit_batch_operations_cron_ready';
    if ($cached = $this->cache->get($cache_id)) {
      // Cache has it so use it.
      $batch_operations_with_cron = $cached->data;
    }
    else {
      // Not in cache, so build it and cache it.
      $all_batch_operations = $this->getBatchOperations($include_test_files);
      // Save only those with cron specified.
      foreach ($all_batch_operations as $batch_operation) {
        $batch_op = $this->getBatchOperationClass($batch_operation);
        if (!empty($batch_op->getCronTiming())) {
          $batch_operations_with_cron[$batch_operation] = (array) $batch_op->getCronTiming();
        }
      }
      $this->cache->set($cache_id, $batch_operations_with_cron, CACHE::PERMANENT);
    }

    return $batch_operations_with_cron;
  }

  /**
   * Checks config to see if running BatchOperations by cron is enabled.
   *
   * @return bool
   *   TRUE if enabled, FALSE otherwise.
   */
  public function isCronEnabled() : bool {
    return $this->cboConfig->get('cron_enabled') ?? FALSE;
  }

  /**
   * Determines the batch size for the operation.
   *
   * This is establishing the default value. It currently has the framework
   * to have an impact, but has no impact because there is no example of how to
   * make use of it (loading a batch size of items all at once).  We may bring
   * this back but right now it should not be used.
   *
   * @return int
   *   For now, 1 is the default return.
   */
  public function getBatchSize(): int {
    return 1;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllowOnlyOneCompleteRun(): bool {
    // Sets this to FALSE by default.
    return FALSE;
  }

}
