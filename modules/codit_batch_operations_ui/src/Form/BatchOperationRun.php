<?php

namespace Drupal\codit_batch_operations_ui\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Form to initiate the running of a BatchOperation.
 */
class BatchOperationRun extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'batch_operation_run';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $build_info = $form_state->getBuildInfo();
    $batch_operation_class_name = $build_info['args'][0] ?? NULL;
    $batch_operation_title = $build_info['args'][1] ?? NULL;

    $form['class_name'] = [
      '#type' => 'hidden',
      '#value' => $batch_operation_class_name,
    ];

    $form['skip'] = [
      '#type' => 'radios',
      '#id' => 'allow-skip',
      '#title' => $this->t('Method'),
      '#title_display' => 'before',
      '#description_display' => 'before',
      '#description' => $this->t('Choose how to handle errors if they occur when it runs:'),
      '#default_value' => NULL,
      '#required' => TRUE,
      '#options' => [
        'skip' => $this->t('Gracefully skip any errors and keep processing the remaining items.'),
        'fail' => $this->t("Fail on error and don't process any remaining items."),
      ],
    ];

    $form['run'] = [
      '#type' => 'submit',
      '#value' => $this->t('Run @title', ['@title' => $batch_operation_title]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // The validations for required, and access are already covered.
    // Validation that the class exists as a proper BatchOperation is happening
    // in the confirmation form.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $parameters = [
      'batch_operation_name' => $this->getClassShortName($form_state->getValue('class_name')),
      'skip' => $form_state->getValue('skip'),
    ];
    $path = Url::fromRoute('codit_batch_operations_ui.operation.confirm', $parameters)->toString();
    $response = new RedirectResponse($path);
    $response->send();
  }

  /**
   * Takes a full classname, and returns just the class name without path.
   *
   * @param string $full_class_name
   *   The complete name of a class.
   *
   * @return string
   *   The short name of the class
   */
  protected function getClassShortName(string $full_class_name) : string {
    $class_parts = explode('\\', $full_class_name);
    return end($class_parts);
  }

}
