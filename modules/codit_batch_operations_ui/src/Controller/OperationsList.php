<?php

namespace Drupal\codit_batch_operations_ui\Controller;

use Drupal\codit_batch_operations\BatchOperationsFilesTrait;
use Drupal\codit_batch_operations\Entity\BatchOpLog;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Utility\TableSort;

/**
 * Class Operations Controller.
 *
 * @package Drupal\codit_batch_operations_ui\Controller
 */
class OperationsList extends OperationsBase {

  use BatchOperationsFilesTrait;

  /**
   * Build the Batch Operation table list render array.
   *
   * @return array
   *   A render array for the Batch Operation table.
   */
  public function listOperations() {
    $operations = $this->getBatchOperations(FALSE);
    $rows = $this->assembleRows($operations);

    $header = [
      ['data' => $this->t('Script name and description'), 'field' => 'title', 'sort' => 'asc'],
      ['data' => $this->t('Last run date'), 'field' => 'last-run-date'],
      ['data' => $this->t('Last run method'), 'field' => 'last-run-method'],
      ['data' => $this->t('Last run by'), 'field' => 'last-run-by'],
      ['data' => $this->t('Run instances'), 'field' => 'run-instances'],
      ['data' => $this->t('Status of last run'), 'field' => 'status-of-last-run'],
    ];

    $order = TableSort::getOrder($header, $this->request);
    $direction = TableSort::getSort($header, $this->request);
    $rows = $this->sortRows($rows, $order['sql'], $direction);

    $form = [];
    $description = $this->t('This table contains scripts that can be run on your system.') . ' ';
    $description .= $this->t('Each script includes the name, description, date and status of the last run, and if the script can be run one time only.') . ' ';
    $description .= $this->t('Select the script name to view the detailed logs for all previous runs.');
    $form['description'] = [
      '#type' => 'markup',
      '#markup' => "<p class=\"description\">$description</p>",
    ];
    $form['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#caption' => $this->t('Available Batch Operations'),
      '#rows' => $rows,
      '#empty' => $this->t('No batch operations found.'),
    ];
    $form['pager'] = [
      '#type' => 'pager',
    ];

    return $form;
  }

  /**
   * Assemble the list rows, one per BatchOperation.
   *
   * @param array $operations
   *   An array of operations.
   *
   * @return array
   *   An array of rows.
   */
  protected function assembleRows(array $operations): array {
    $rows = [];
    foreach ($operations as $operation) {
      $class_uri = $this->getNamespacedClassName($operation);
      $script = $this->getBatchOperationClass($operation);
      $batchOpLog = new BatchOpLog([], 'batch_op_log');
      $batOpLogIds = $batchOpLog->getBatchOpLogIds(trim($class_uri, '\\'));
      $most_recent_run_log = $batchOpLog->getMostRecentBatchOpLog(trim($class_uri, '\\'));
      $class_name = substr(strrchr(get_class($script), '\\'), 1);
      $total_items = $script->getTotalItemCount();
      $title_vars = [
        '@class_name' => $class_name,
        '@description' => $script->getTitle(),
        '@count' => count($batOpLogIds),
      ];
      $instances_text = ($script->getAllowOnlyOneCompleteRun()) ? new FormattableMarkup('@count times</br>(Can run once only)', $title_vars) : new FormattableMarkup('@count times', $title_vars);
      $row = [
        'title' => new FormattableMarkup('<a href="./operations/@class_name">@class_name</a> </br> @description', $title_vars),
        'last-run-date' => ($most_recent_run_log) ? $this->dateFormatter->format($most_recent_run_log->get('last')->value, 'medium') : '-',
        'last-run-method' => ($most_recent_run_log) ? $most_recent_run_log->get('executor')->value : '-',
        'last-run-by' => ($most_recent_run_log) ? $most_recent_run_log->get('user_id')->entity->get('name')->value : '-',
        'run-instances' => $instances_text,
        'status-of-last-run' => $this->buildCompleteStatus($most_recent_run_log, $total_items),
      ];

      $rows[$operation] = $row;
    }

    return $rows;
  }

}
