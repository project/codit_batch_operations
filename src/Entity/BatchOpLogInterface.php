<?php

namespace Drupal\codit_batch_operations\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Url;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining BatchOpLog entities.
 *
 * @ingroup batch_op_log
 */
interface BatchOpLogInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Gets the BatchOpLog name.
   *
   * @return string
   *   Name of the BatchOpLog entity.
   */
  public function getName(): string;

  /**
   * Sets the BatchOpLog name.
   *
   * @param string $name
   *   The BatchOpLog name.
   *
   * @return \Drupal\codit_batch_operations\Entity\BatchOpLogInterface
   *   The called BatchOpLog entity.
   */
  public function setName(string $name): BatchOpLogInterface;

  /**
   * Sets the Log to do not save.  To prevent saving a non-started operation.
   *
   * @return \Drupal\codit_batch_operations\Entity\BatchOpLogInterface
   *   The called BatchOpLog entity.
   */
  public function setDoNotSave(): BatchOpLogInterface;

  /**
   * Checks to see if the BatchOpLog can be saved.
   *
   * @return bool
   *   TRUE if the BatchOpLog entity can be saved, FALSE otherwise.
   */
  public function canSave(): bool;

  /**
   * Sets the executor property.
   *
   * @param string $executor
   *   The id of the executor.
   *
   * @return \Drupal\codit_batch_operations\Entity\BatchOpLogInterface
   *   The called BatchOpLog entity.
   */
  public function setExecutor(string $executor): BatchOpLogInterface;

  /**
   * Appends a message to the current BatchOpLog log.
   *
   * @param string $message
   *   The message to append.
   *
   * @return \Drupal\codit_batch_operations\Entity\BatchOpLogInterface
   *   The BatchOpLog.
   */
  public function appendLog(string $message): BatchOpLogInterface;

  /**
   * Appends an error to the current BatchOpLog errors.
   *
   * @param string $message
   *   The message to append.
   *
   * @return \Drupal\codit_batch_operations\Entity\BatchOpLogInterface
   *   The BatchOpLog.
   */
  public function appendError($message): BatchOpLogInterface;

  /**
   * Getter for errors.
   *
   * @return ?string
   *   The errors logged.
   */
  public function getErrors(): ?string;

  /**
   * Checks to see if there are errors.
   *
   * @return bool
   *   TRUE if there were errors logged, FALSE otherwise.
   */
  public function hasErrors(): bool;

  /**
   * Checks that the log has recorded completion.
   *
   * @return bool
   *   TRUE if completed, FALSE otherwise.
   */
  public function getCompleted(): bool;

  /**
   * Gets the BatchOpLog creation timestamp.
   *
   * @return int
   *   Creation timestamp of the BatchOpLog.
   */
  public function getCreatedTime();

  /**
   * Sets the BatchOpLog creation timestamp.
   *
   * @param int $timestamp
   *   The BatchOpLog creation timestamp.
   *
   * @return \Drupal\codit_batch_operations\Entity\BatchOpLogInterface
   *   The called BatchOpLog entity.
   */
  public function setCreatedTime(int $timestamp): BatchOpLogInterface;

  /**
   * Gets the time that the BatchOpLog was last updated.
   *
   * @return ?string
   *   The last updated time of the BatchOpLog, or NULL if never updated.
   */
  public function getLastUpdatedTime(): ?string;

  /**
   * Gets the drupal URL for the BatchOpLog.
   *
   * @return string
   *   URL for the current BatchOpLog.
   */
  public function getUrl(): Url;

  /**
   * Gets the entity ID for all Batch operation logs for this BatchOperation.
   *
   * @param string $batch_operation_name
   *   The full name of the Batch Operation to lookup including Drupal\ path.
   *
   * @return array
   *   An array of BatchOpLog entity ids.
   */
  public function getBatchOpLogIds(string $batch_operation_name): array;

  /**
   * Retrieves the most recent BatchOpLog for this BatchOperation.
   *
   * @param string $batch_operation_name
   *   The full name of the Batch Operation to lookup including Drupal/ path.
   *
   * @return BatchOpLog
   *   Most recent BatchOpLog for this BatchOperation, or NULL if none exist.
   */
  public function getMostRecentBatchOpLog(string $batch_operation_name): ?BatchOpLog;

  /**
   * Retrieves all BatchOpLogs for this BatchOperation.
   *
   * @param string $batch_operation_name
   *   The full name of the Batch Operation to lookup including Drupal/ path.
   *
   * @return array
   *   An array of BatchOpLogs for this BatchOperation, or [] if none exist.
   */
  public function getRelatedBatchOpLogs(string $batch_operation_name) : array;

}
