<?php

namespace Drupal\codit_batch_operations;

/**
 * Required methods that need implementing for BatchScripts.
 */
interface BatchScriptInterface {

  /**
   * Get the string for the title of the batch.  Needed for the UI.
   *
   * @return string
   *   The human name of the batch.  Good idea to match the filename of script.
   */
  public function getTitle():string;

  /**
   * Defines the message to use when complete.
   *
   * The message can include the tokens '@completed' and '@total'.
   *
   * @return string
   *   The message to end the script with.
   */
  public function getCompletedMessage(): string;

  /**
   * Defines whether a BatchOperation can only have one completed run.
   *
   *   It could have multiple incomplete runs because they pick up where they
   * leave off.  But can only be run once to completion.
   *
   * @return bool
   *   True if it can only have one complete run, FALSE otherwise.
   */
  public function getAllowOnlyOneCompleteRun(): bool;

  /**
   * Optionally describe details about the purpose or history of this script.
   *
   * Suggestions: Answer any who, what, when, where, why kind of questions that
   * you can.  Link to the issue that resulted the script's creation.  Concerns,
   * risks, and special conditions are all good to include.
   *
   * @return string
   *   The description text to be displayed in the UI.
   */
  public function getDescription(): string;

  /**
   * Optionally name the main type of thing being operated on.
   *
   * Suggestions: node, term, menu_item, block, bundle name, user, etc.
   *
   * If you do not define this, it defaults to 'item'.
   *
   * @return string
   *   The string that will be used to prefix items in the logging.
   */
  public function getItemType(): string;

  /**
   * Gets the batch size to run.
   *
   * When in doubt return 1.  Batches are only useful to save read operations on
   * large numbers of items to process.
   *
   * @return int
   *   The number of items in the batch.
   */
  // @codingStandardsIgnoreStart
  // Removing this until its purpose and function is better defined.
  // public function getBatchSize(): int;
  // @codingStandardsIgnoreEnd

  /**
   * Gather the items to process.
   *
   * This is whatever query magic is needed to
   * get the set of items you want to process with your script.  If your set is
   * fewer than 100 items, it is safe to load them all and include them in this
   * array, otherwise it is safer to have processOne() do the loading.
   *
   * @return array
   *   An array of items to process could be keyed like nid => nid
   *   or nid => node.
   */
  public function gatherItemsToProcess(): array;

  /**
   * Gets and optionally sets the total item count of all items to be processed.
   *
   * @return int
   *   The total amount of items to be processed.
   */
  public function getTotalItemCount(): int;

  /**
   * The magic that processes one of the items.
   *
   * @param string $key
   *   The key from the Items to Process.
   * @param mixed $item
   *   The element value from the Items to Process.
   * @param array $sandbox
   *   The sandbox by reference. Used for keeping state in the batch.
   *
   * @return string
   *   The message that processing one item should declare.
   */
  public function processOne(string $key, mixed $item, array &$sandbox): string;

  /**
   * Optional method that is called once BEFORE the run of the BatchOperation.
   *
   * This can be used to turn on maintenance mode, disable indexing, or
   * anything else that needs to be done before running the BatchOperation.
   *
   * @param array $sandbox
   *   The sandbox by reference. Used for keeping state in the batch.
   *
   * @return string
   *   A message that will be added to the BatchOpLog
   */
  public function preRun(&$sandbox): string;

  /**
   * Optional method that is called once AFTER the run of the BatchOperation.
   *
   * This can be used to undo anything that was done in preRun(). Like
   * turn off maintenance mode, re-enable indexing, or
   * anything else that needs to be done after running the BatchOperation.
   *
   * @param array $sandbox
   *   The sandbox by reference. Used for keeping state in the batch.
   *
   * @return string
   *   A message that will be added to the BatchOpLog
   */
  public function postRun(&$sandbox): string;

  /**
   * Optional. Get the timing string for having this BatchOperation run on cron.
   *
   * These can be stacked to have multiple cron definitions example:
   * return ['on the 1st of January', 'on the 4th of July'];
   *
   * @return string string|string[]
   *   A human readable cron string or array of strings matching
   *   - 'every 10 minutes'
   *   - 'every 2 days'
   *   - 'every 2 weeks
   *   - 'every 3 months'
   *   - 'every 2 years'
   *   - 'every 10 days after 16:00'
   *   - 'on the 6th of every 1 month'
   *   - 'on the 15th of every 1 months after 16:00'
   *   - 'on the 4th of July'
   *   - 'on the 4th of July after 14:00'
   */
  public function getCronTiming(): string | array;

}
