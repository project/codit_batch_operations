<?php

namespace Drupal\codit_batch_operations_ui\Controller;

use Drupal\codit_batch_operations\Entity\BatchOpLogInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\State\State;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class Operations Controller.
 *
 * @package Drupal\codit_batch_operations_ui\Controller
 */
class OperationsBase extends ControllerBase {
  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  public $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public $entityTypeManager;

  /**
   * Module handler interface.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  public $moduleHandler;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The state api service.
   *
   * @var \Drupal\Core\State\State
   */
  public $state;

  /**
   * The configuration for codit_batch_operations.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $cboConfig;

  /**
   * The core form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;


  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  public $logger;

  /**
   * Constructs the BatchOperations service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder interface.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler for determining which modules are installed.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\State\State $state
   *   The State API service.
   */
  final public function __construct(
    ConfigFactoryInterface $config_factory,
    DateFormatterInterface $date_formatter,
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeManagerInterface $entity_type_manager,
    FormBuilderInterface $form_builder,
    LoggerChannelFactoryInterface $logger_factory,
    ModuleHandlerInterface $module_handler,
    RequestStack $request_stack,
    State $state,
  ) {
    $this->configFactory = $config_factory;
    $this->dateFormatter = $date_formatter;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->formBuilder = $form_builder;
    $this->logger = $logger_factory->get('codit_batch_operations');
    $this->moduleHandler = $module_handler;
    $this->request = $request_stack->getCurrentRequest();
    $this->state = $state;
    $this->cboConfig = $this->configFactory->getEditable('codit_batch_operations.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('date.formatter'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('form_builder'),
      $container->get('logger.factory'),
      $container->get('module_handler'),
      $container->get('request_stack'),
      $container->get('state'),
    );
  }

  /**
   * Sort the rows based on a column/field.
   *
   * @param array $rows
   *   The rows that make up the table.
   * @param string $field_key
   *   The name of the field / column to sort by.
   * @param string $direction
   *   The direction to sort (desc or asc).
   *
   * @return array
   *   The sorted array.
   */
  protected function sortRows(array $rows, string $field_key, string $direction): array {
    $direction = ($direction === 'desc') ? SORT_DESC : SORT_NATURAL;
    $sort_column = array_column($rows, $field_key);
    array_multisort($sort_column, $direction, $rows);
    return $rows;
  }

  /**
   * Convert duration times into closest unit.
   *
   * @param array $rows
   *   An array of BatchOpLog rows.
   *
   * @return array
   *   The same array passed in, but with duration column converted.
   */
  protected function convertDuration(array $rows) : array {
    foreach ($rows as $key => $row) {
      $duration = $row['duration'];
      switch (TRUE) {
        case $row['duration'] <= 60:
          $rows[$key]['duration'] = $this->t('@duration seconds', ['@duration' => $duration]);
          break;

        case $row['duration'] <= 3600:
          $duration = round($duration / 60, 1);
          $rows[$key]['duration'] = $this->t('@duration minutes', ['@duration' => $duration]);
          break;

        default:
          $duration = round($duration / 3600, 1);
          $rows[$key]['duration'] = $this->t('@duration hours', ['@duration' => $duration]);
          break;
      }

    }
    return $rows;
  }

  /**
   * Convert the run time to a date after sorting happens.
   *
   * @param array $rows
   *   An array of BatchOpLog rows.
   *
   * @return array
   *   The same array passed in, but with run-date column converted.
   */
  protected function convertDate(array $rows) : array {
    foreach ($rows as $key => $row) {
      if (!empty($row['run-date'])) {
        $date = $row['run-date'];
        $rows[$key]['run-date'] = $this->dateFormatter->format($date, 'medium');
      }
      else {
        $rows[$key]['run-date'] = $this->t('Not recorded.');
      }
    }
    return $rows;
  }

  /**
   * Build the status to display for the most run.
   *
   * @param \Drupal\codit_batch_operations\Entity\BatchOpLogInterface $batch_op_log
   *   The most recent BatchOpLog run for this BatchOp.
   * @param int $current_item_count
   *   The current item count.
   *
   * @return \Drupal\Component\Render\FormattableMarkup
   *   The status report for the last run.
   */
  protected function buildCompleteStatus(?BatchOpLogInterface $batch_op_log, int $current_item_count): FormattableMarkup {
    // There are three possible cases here:
    // 1) There is a batch_op_log for a run
    // 2) There is a batch_op_log for a run, but it predates item count fields.
    // 3) There is no previous run.
    // If log exists, get the count from it, if not, use the current item count.
    $total_items = ($batch_op_log) ? $batch_op_log->get('total_items')->value : $current_item_count;
    $last_item_completed = ($batch_op_log) ? $batch_op_log->get('last_item_processed')->value : 0;
    $completed = ($batch_op_log) ? $batch_op_log->get('completed')->value : FALSE;
    $errors = ($batch_op_log) ? !empty($batch_op_log->getErrors()) : FALSE;
    $vars = [
      '@last_item_completed' => is_null($last_item_completed) ? $this->t('unknown') : $last_item_completed,
      '@total_items' => is_null($total_items) ? 0 : $total_items,
      '@percent' => empty($total_items) ? '-' : round(($last_item_completed / $total_items) * 100),
      '@units' => $this->t('items processed'),
    ];
    $show_progress = TRUE;

    switch (TRUE) {
      case empty($batch_op_log):
        $vars['@msg'] = $this->t('Not run');
        $vars['@units'] = $this->t('items');
        $vars['@classes'] = 'not-run';
        break;

      case ($completed && !$errors):
        $vars['@msg'] = $this->t('Completed');
        $vars['@classes'] = 'complete';
        break;

      case ($completed && $errors):
        $vars['@msg'] = $this->t('Completed with errors');
        $vars['@classes'] = 'complete errors';
        break;

      case (!$completed && !$errors):
        $vars['@msg'] = $this->t('Incomplete', $vars);
        $vars['@classes'] = 'incomplete';
        break;

      case (!$completed && $errors):
        $vars['@msg'] = $this->t('Incomplete with errors', $vars);
        $vars['@classes'] = 'incomplete errors';
        break;

      default:
        $vars['@msg'] = $this->t('Not run');
        $vars['@units'] = $this->t('items');
        $vars['@classes'] = 'not-run';
        break;
    }

    if (is_null($last_item_completed)) {
      // This log predates beta8 where count fields were added to BatchOpLogs.
      $show_progress = FALSE;
    }

    if ($show_progress) {
      return new FormattableMarkup('<div class="cbo_status_group @classes"><div class="cbo_status">@msg</div><progress class="cbo_progress" value="@last_item_completed" max="@total_items"> @percent% </progress></br> <div class="cbo_count">@last_item_completed/@total_items @units</div><div class="cbo_percent">@percent%</div></div>', $vars);
    }
    else {
      return new FormattableMarkup('<div class="cbo_status_group @classes"><div class="cbo_status">@msg</div>', $vars);
    }
  }

}
