<?php

namespace Drupal\codit_batch_operations\cbo_scripts;

use Drupal\codit_batch_operations\BatchOperations;
use Drupal\codit_batch_operations\BatchScriptInterface;

/**
 * This is a Batch Operation that should fail calmly not catastrophically.
 *
 * Sometimes we create a batch operation that moves content and removes the old.
 * In these cases it is possible to have a gatherItemsToProcess() that now tries
 * to gather things that do not exist. In this case, we should be told why it
 * fails.
 */
class TestErrorOnGatheringItems extends BatchOperations implements BatchScriptInterface {

  /**
   * {@inheritdoc}
   */
  public function getTitle():string {
    return 'Try to do something, but fail on gathering items.';
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription():string {
    $description = <<<ENDHERE
    This is intended as an example for writing a BatchOperation that tries to
    gather items from an entity that no longer exists using a field that no
    longer exists.
    ENDHERE;
    return $description;
  }

  /**
   * {@inheritdoc}
   */
  public function getCompletedMessage(): string {
    // This message can include the tokens '@completed' and '@total'.
    return '@completed times we processed a ghost of our former site out of @total times.';
  }

  /**
   * {@inheritdoc}
   */
  public function gatherItemsToProcess(): array {
    // We are intentionally making an error here by looking for something that
    // does not exist. It should cause an error on the BatchOperation list UI
    // page and on the trying to run it. If you see an error that is good,
    // because is means the error was caught and did not White screen of death.
    $query = $this->entityTypeManager->getStorage('node')->getQuery();
    $items = $query
      ->condition('type', 'yorick_parts')
      ->accessCheck(FALSE)
      ->condition('non_existent_field_to_cause_intentional_error-PLEASE-IGNORE', 'skull', '=')
      ->execute();
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function processOne(string $key, mixed $item, array &$sandbox): string {
    // Do some things in here, then return a message about what was done.
    // If you return a non-empty message, it will get logged in the BatchOpLog.
    // If you were doing a big process and wanted to add to the log or errors,
    // you can log specifically as you go.
    return "I spoke 'Alas poor Yorick, I knew him well.' '{$item}'";
  }

}

// @codingStandardsIgnoreStart
// Example of how to run this batch from a hook_update_n()
/**
 * Run a script example with coconuts.
 */
// function my_module_update_9012(&$sandbox) {
//  $script = \Drupal::classResolver('\Drupal\codit_batch_operations\cbo_scripts\TestErrorOnGatheringItems');
//  return $script->run($sandbox, 'hook_update');
// }
//
//
// Run with drush:
// drush codit-batch-operations:run TestErrorOnGatheringItems
// @codingStandardsIgnoreEnd
