<?php

namespace Drupal\codit_batch_operations_ui\Form;

use Drupal\codit_batch_operations\BatchOperations;
use Drupal\codit_batch_operations\BatchScriptInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines a confirmation form to confirm running of a batch operation.
 */
class BatchOperationRunConfirmation extends ConfirmFormBase {

  /**
   * The BatchOperation to run.
   *
   * @var \Drupal\codit_batch_operations\BatchOperations
   */
  protected $batchOperation;

  /**
   * The full class name with path of the BatchOperation to run.
   *
   * @var string
   */
  protected $batchOperationName;

  /**
   * The skip method to use when running the Batch Operation.
   *
   * @var bool
   */
  protected $skip;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $build_info = $form_state->getBuildInfo();
    $this->batchOperationName = $build_info['args'][0] ?? NULL;
    // @phpstan-ignore-next-line (Can not use dependency injection for this.)
    $this->batchOperation = \Drupal::classResolver($this->batchOperationName);
    $skip = $build_info['args'][1] ?? NULL;
    $this->skip = ($skip === 'skip') ? TRUE : FALSE;
    $vars = [
      '@count' => $this->batchOperation->getTotalItemCount(),
    ];
    $form['explanation'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t("You are about to run an operation on @count items. Other batch operations can't be run while this operation is in progress. Select <b>Yes, run operation now</b> to run.", $vars),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batch = [
      'operations' => [
        [[$this->batchOperationName, 'runBatchByUi'], [$this->batchOperation, $this->skip]],
      ],
      'progressive' => TRUE,
      'finished' => [$this->batchOperationName, 'finishedBatchByUi'],
      'title' => t('Processing Batch @batch_name', ['@batch_name' => $this->batchOperation->getShortName()]),
      'init_message' => t('@class Batch is starting.', ['@class' => $this->batchOperation::class]),
      'progress_message' => t('@class in progress. (elapsed time: @elapsed) (estimated time remaining: @estimate)', ['@class' => $this->batchOperation::class]),
      'error_message' => t('@class has encountered an error.', ['@class' => $this->batchOperation::class]),
    ];
    batch_set($batch);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // The validations for required, and access are already covered.
    // Validation for the skip method is also already covered by the route.
    $msg_vars = [
      '@class' => $this->batchOperation::class,
    ];
    if (!($this->batchOperation instanceof BatchOperations)) {
      $message = $this->t("The class '@class' does not seem to be an instance of BatchOperations and can not be run.", $msg_vars);
      $form_state->setErrorByName('title', $message);
      return;
    }

    if (!($this->batchOperation instanceof BatchScriptInterface)) {
      $message = $this->t("The class '@class' does not implement the BatchScriptInterface interface and can not be run.", $msg_vars);
      $form_state->setErrorByName('title', $message);
      return;
    }

    $script_running = $this->batchOperation->getWhatsRunning();
    if (!empty($script_running)) {
      $vars = [
        '@running_script' => $script_running,
        '@drush_reset' => 'drush codit-batch-operations:running --reset',
      ];
      $message = $this->t("The Batch Operation '@running_script' appears to be running and prevents this one from running at this time.", $vars) . ' ';
      $message .= $this->t("If you think this is a mistake, run '@drush_reset' to reset the state or visit the settings page.", $vars);
      $form_state->setErrorByName('title', $message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return 'confirm_batch_operation_run';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $route_params = [
      'batch_operation_name' => $this->batchOperation->getShortName(),
    ];
    return new Url('codit_batch_operations_ui.operation', $route_params);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $vars = [
      '@name' => $this->batchOperation->getShortName(),
    ];
    return $this->t('Run batch operation @name?', $vars);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Yes, run operation now');
  }

}
