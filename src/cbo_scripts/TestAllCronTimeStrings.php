<?php

namespace Drupal\codit_batch_operations\cbo_scripts;

use Drupal\codit_batch_operations\BatchOperations;
use Drupal\codit_batch_operations\BatchScriptInterface;
use Drupal\codit_batch_operations\Cron\CronManager;

/**
 * A test and example Batch operation script to show processing.
 */
class TestAllCronTimeStrings extends BatchOperations implements BatchScriptInterface {

  /**
   * {@inheritdoc}
   */
  public function getTitle():string {
    return 'Test all cron string variations and debug to the BatchOplog.';
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription():string {
    $description = <<<ENDHERE
    This does not run on cron but it does test many variations of the allowed
    cron timing strings to and outputs them to the log for debugging.  It is
    mainly meant for maintainers to test cron timing strings.
    ENDHERE;
    return $description;
  }

  /**
   * {@inheritdoc}
   */
  public function getCompletedMessage(): string {
    // This message can include the tokens '@completed' and '@total'.
    return 'Tried @completed out of @total cron strings. See the log for debug and manual validation.';
  }

  /**
   * {@inheritdoc}
   */
  public function getItemType(): string {
    return 'sample_cron_timing';
  }

  /**
   * {@inheritdoc}
   */
  public function gatherItemsToProcess(): array {
    // These should all be valid time strings.
    $cron_strings = [
      'every 10 minutes',
      'every 2 days',
      'every 1 day after 00:00',
      'every 10 days after 16:00',
      'every 2 weeks',
      'every 2 weeks after 02:00',
      'every 3 months',
      'every 3 months after 13:00',
      'every 2 years',
      'on the 6th of every 2 month',
      'on the 15th of every 1 months after 16:00',
      'on the 1st of January after 01:00',
      'on the 10th of Feb after 12:45',
      'on the 4th of July',
      'on the 4th of July after 14:00',
      'on the 31st of December after 12:00',
      'on the 15th of every 4 months after 02:00',
    ];

    return $cron_strings;
  }

  /**
   * {@inheritdoc}
   */
  public function processOne(string $key, mixed $item, array &$sandbox): string {
    // We are going to validate the cron time string.
    $cron_manager = new CronManager($this->getShortName(), $item, $this);
    $debug = $cron_manager->getDebug();
    return print_r($debug, TRUE);
  }

}

// @codingStandardsIgnoreStart
// Example of how to run this batch from a hook_update_n()
/**
 * Run a script example with coconuts.
 */
// function my_module_update_9012(&$sandbox) {
//  $script = \Drupal::classResolver('\Drupal\codit_batch_operations\cbo_scripts\TestAllCronTimeStrings');
//  return $script->run($sandbox, 'hook_update');
// }
//
//
// Run with drush:
// drush codit-batch-operations:run TestAllCronTimeStrings
// @codingStandardsIgnoreEnd
